#!/usr/bin/python3

"""
Generate the table showing the polynomial fits to the mean historical population
size data.
"""

import argparse
from OriginFixation import Resources
from Presentation import LongTable, Format

parser = argparse.ArgumentParser(description=__doc__)

args = parser.parse_args()

# Retrieve the population curve
popcurve = Resources.retrieve(Resources.populationcurve())

# Create the table

table = LongTable.LongTable(Resources.table('polycoeffs'),
    ['Coefficient'] + ['$c_{'+str(x)+'}$' for x in range(len(popcurve.poppolyfit))],
    caption = r"""
Coefficients in the polynomial fit $c_0 + c_1 t + c_2 t^2 + c_3 t^3 + c_4 t^4$
to the function $\ln (g(t))$ in (\ref{lnpopsi}) obtained by least-squares minimisation.
    """,
    label = 'stab:polycoeffs'
)

table.row(['Value'] + [ Format.to_precision(x,3) for x in popcurve.poppolyfit ])
table.finalise()
print('Output written in {where}'.format(where=Resources.table('polycoeffs')))
