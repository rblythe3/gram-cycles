/**
  * Estimate the two-sided p value of a contingency table by Monte Carlo sampling.
  * Provide the contigency table on the command line in row-major order,
  * and specify the number of rows and columns with the -r and -c options.
  * A MC update corresponds to increasing and decreasing two counts each
  * in such a way that the row and column sums remain invariant. Successive
  * samples are likely to be correlated, so you will want a large number of samples
  * for a good estimate.
  *
  * This has been validated against Fisher's exact test for 2x2 tables.
  */

#include <iostream>
#include <vector>
#include <utility>
#include <random>
#include <cmath>

#include "CLI11.hpp"

class pair_distribution {
  // Samples pairs of distinct integers in [0,1,...,n)
  std::uniform_int_distribution<unsigned> first, second;

public:
  pair_distribution(unsigned n) : first(0,n-1), second(0,n-2) { }

  template<typename Engine>
  std::pair<unsigned,unsigned> operator() (Engine& rng) {
    unsigned i = first(rng), j = second(rng);
    return std::make_pair(i, j<i ? j : j+1);
  }
};

int main(int argc, char* argv[]) {

  unsigned rows = 2;
  unsigned cols = 2;
  std::vector<unsigned> counts;
  unsigned burnin = 0;
  unsigned samples = 1000000;
  unsigned delta = 1;
  unsigned seed = 0;

  unsigned bins = 0;

  CLI::App app{"Estimate of p-value for a contingency table"};

  app.add_option("-r,--rows", rows, "Number of rows in the table [2]");
  app.add_option("-c,--cols", cols, "Number of columns in the table [2]");
  app.add_option("-b,--burnin", burnin, "Number of MC steps before accumulating [0]");
  app.add_option("-s,--samples", samples, "Number of MC samples that contribute to the estimate [1000000]");
  app.add_option("-d,--delta", delta, "Number of MC steps between contributing samples [1]");
  app.add_option("-S,--seed", seed, "Random number seed [0]");
  app.add_option("counts", counts, "Table counts (row-major order: n_11, n_12, ...)");

  CLI11_PARSE(app, argc, argv);

  if(counts.size() != rows * cols) {
    std::cerr << "Must provided a set of counts of size " << rows*cols << std::endl;
    return 1;
  }

  // Output the contingency table, so we know that we've done it right!
  std::cout << "Contingency table:" << std::endl;
  for(unsigned i=0; i<rows; ++i) {
    for(unsigned j=0; j<cols; ++j) {
      std::cout << counts[i*cols+j] << "\t";
    }
    std::cout << std::endl;
  }

  // Calculate the base likelihood
  double base = 0.0;
  for(auto c: counts) base -= std::lgamma(c+1);

  // Fire up a random number generator
  std::mt19937 rng(seed);

  // Construct distributions to sample pairs of rows and columns
  pair_distribution rowpair(rows);
  pair_distribution colpair(cols);

  // Uniform variate
  std::uniform_real_distribution<double> uniform;

  unsigned extreme = 0; // Count of tables which are more extreme than those observed

  std::vector<unsigned> histogram(bins);

  for(unsigned n=0; n<burnin + samples*delta; ++n) {
    auto swapr = rowpair(rng);
    auto swapc = colpair(rng);
    // Get references to the four elements we might change, so we can change them easily if required
    unsigned& c11 = counts[swapr.first * cols + swapc.first];
    unsigned& c12 = counts[swapr.first * cols + swapc.second];
    unsigned& c21 = counts[swapr.second * cols + swapc.first];
    unsigned& c22 = counts[swapr.second * cols + swapc.second];

    // Numerator and denominator of Metropolis-Hastings acceptance probability
    unsigned num = c12*c21, den = (c11+1)*(c22+1);
    if(num >= den || den * uniform(rng) < num) {
      ++c11; --c12; --c21; ++c22;
    }

    // Calculate likelihood of this table relative to the base table; if smaller, add to count
    double like = 0.0;
    for(auto c: counts) like -= std::lgamma(c+1);

    // We consider a sample as extreme if it is at least as unlikely as the base
    // we only include samples after burnin; and at intervals of delta thereafter
    if(like <= base && n>=burnin && (n-burnin)%delta == 0) ++extreme;
  }

  double pvalue = double(extreme) / double(samples);
  std::cout << "p-value estimate based on " << samples << " samples: " << pvalue << std::endl;

  return 0;
}
