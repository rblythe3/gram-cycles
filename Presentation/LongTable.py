"""
Tabulate data in a LaTeX longtable environment.
"""

class LongTable(object):

    def __init__(self, file, columns, hline=1, caption=None, label=None, vruleskip=1):
        """
        Create a longtable in the named file.
        Columns are headed with the LaTeX source in the columns list.
        One or more hlines follow if hline>0.
        A caption can be inserted in an appropriate place, and the table
        can be given crossreferencing label.
        Increase vruleskip if you don't want a | between all columns; 0 = none
        """
        self.fh = open(file, 'w')
        self.columns = len(columns)
        self.caption = caption
        self.label = label
        if vruleskip < 1 or vruleskip > self.columns:
            vruleskip = self.columns
        fmt = ('|'.join(['l'*vruleskip]*(self.columns//vruleskip))) + ('l'*(self.columns%vruleskip))
        self.fh.write(r'\begin{longtable}{' + fmt + '}\n')
        self.row(columns, hline)
        self.fh.write(r'\endhead' + '\n')

    def row(self, cells, hline=0):
        """
        Add a row. cells is a list with one element of LaTeX source per cell.
        One or more hline(s) follow if hline > 0
        """
        if len(cells) > self.columns:
            raise ValueError('Too many cells in table row')
        cells += ['']*(self.columns - len(cells))
        self.fh.write(' & '.join(cells))
        self.fh.write(r' \\' + (''.join([r'\hline']*hline)) + '\n')

    def splitrows(self, *rows, **kwargs):
        """
        Add a set of rows, splitting the last cell of the first row onto the
        next row. Example:
        splitrows(['1','2a'],['2b','3'])
        will make a table formatted as
        | 1 | 2a |   |
        |   | 2b | 3 |
        The keyword arg hline adds hlines
        """
        leading = 0
        for row in rows[:-1]:
            self.row([]*leading + row)
            leading = len(row)-1
        self.row(['']*leading + rows[-1], hline = kwargs['hline'] if 'hline' in kwargs else 0)

    def finalise(self):
        """
        Add footers and close the output file
        """
        if self.caption is not None:
            self.fh.write(r'\caption{')
            if self.label is not None:
                self.fh.write(r'\label{' + self.label + '}')
            self.fh.write(self.caption + '}\n')
        self.fh.write(r'\end{longtable}' + '\n')
        self.fh.close()
