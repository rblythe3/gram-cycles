#
# Calibrate the Poisson-Gamma population model against numerical solutions of the
# backward master equation
#

import numpy as np, scipy.special as sps, matplotlib.pyplot as plt, scipy.optimize as spo
from OriginFixation import Resources, Analysis

WF = Analysis.WFBase() # Obtain a single WF engine

def mkplot(N, mu, s, twice=False, target=1, points=50, cutoff=None):

    print("Calibrating N={N} mu={mu} s={s} twice={twox} target={targ}".format(N=N, mu=mu, s=s, twox=twice, targ=target))

    ts,ps = np.loadtxt(Resources.wrightfisher(N=N,mu=mu,s=s,mutate_twice=twice,target=target),unpack=True)
    N = int(N)
    mu, s = map(np.float_, (mu,s))

    # Obtain the PG parameters for the Wright-Fisher model
    alpha,beta,omega = WF.abo(N, mu, s, 1, 1, float('inf'))

    # Mean and standard error of the propagation
    T1 = alpha/beta
    sigT = np.sqrt(alpha/beta/beta)

    print('\tT1={T1} sigT={sT} omega={o} alpha={a} beta={b} int={i}'.format(T1=T1, sT = sigT, o=omega, a=alpha, b=beta, i=omega*alpha/beta))

    # Poisson gamma amplitude
    amp = (beta/(beta-omega))**alpha
    # Rate of a Poisson process with the same mean
    omegaf = omega / (1.0 + omega*T1)

    if target == 2:
        pg = lambda t: sps.gammainc( 2.0*alpha, beta*t) - np.exp(-omega*t) * amp*amp * ( (1.0 + omega*t) * sps.gammainc(2.0*alpha, (beta-omega)*t) - 2.0*alpha*omega/(beta-omega)*sps.gammainc(2.0*alpha+1.0,(beta-omega)*t))
        poi = lambda t: 1.0 - (1.0 + omegaf*t) * np.exp(-omegaf*t)
    else:
        pg = lambda t: sps.gammainc( alpha, beta*t ) - np.exp(-omega*t) * amp * sps.gammainc( alpha, (beta-omega)*t )
        poi = lambda t: 1.0 - np.exp(-omegaf*t)

    # Asymptote
    if twice and target==1:
        asymp = spo.curve_fit(lambda t,a: a*pg(t), ts, ps)[0][0]
        print('\tfit asymptote={}'.format(asymp))
    else:
        asymp = 1.0

    # Create pruned arrays and fit ranges
    pts = np.array(ts)
    pps = np.array(ps)

    if cutoff is not None:
        ks = np.where(pts <= cutoff)
        pts = pts[ks]
        pps = pps[ks]
        fts = np.linspace(0.0, cutoff, 100)
    else:
        fts = np.linspace(0.0, np.max(pts), 100)

    # Prune data
    skip = len(pts)//points
    if skip > 1:
        pts = pts[::skip]
        pps = pps[::skip]

    # Numerical solution, in real space to see deviation from Poisson
    plt.plot(pts,pps,'o',label='Wright-Fisher')

    # Poisson-Gamma fit
    plt.plot(fts, asymp*pg(fts), 'k', lw=2, label='Origin-fixation')

    # Head of the Poisson fit with the same mean
    plt.plot(fts, asymp*poi(fts), ':', label='Poisson')

    # Plot the asymptote, if it is meaningful to do so
    if twice and target==1:
        plt.plot(plt.xlim(), [asymp]*2, '--')

    plt.ylim((0,1))

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--popsize', '-N', default='100', help='Population size [100]')
    parser.add_argument('--mutate', '-m', default='1e-3', help='Mutation rate [1e-3]')
    parser.add_argument('--selection', '-s', default='0', help='Selectrion strength [0]')
    parser.add_argument('--twice', '-x', action='store_true', help='Allow second mutation [off]')
    parser.add_argument('--target', '-T', default='1', choices=['1','2'], help='Fixation state to target: 1|2 [1]')

    parser.add_argument('--points', '-n', default=50, type=int, help='Prune to this number of data points [50]')
    parser.add_argument('--cutoff', '-t', type=float, help='Upper time cutoff')

    args = parser.parse_args()

    plt.figure(1)
    mkplot(args.popsize, args.mutate, args.selection, args.twice, args.target, args.points, args.cutoff)
    plt.xlabel('time, t')
    plt.ylabel('P(t)')
    plt.show()
