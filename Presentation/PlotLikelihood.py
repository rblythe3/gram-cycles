"""
Plot AICC differences and overdispersion statistics.
"""

from OriginFixation import Resources, Summary
import matplotlib.pyplot as plt, numpy as np

def aicc(feature, tag, xaxis='s+', model='wrightfisher', baseline_model='poisson', baseline_tag='baseline', label=None):
    """
    Plot the aicc difference between the specified model and the baseline model.
    xaxis can be one of 's+' (positive selection values), 's-' (negative selection values)
    or 'R:' (interaction rates, with unphysical values dotted).
    Lines can be optionally labelled.
    """

    # Load the baseline data
    baseline = Summary.Summary(Resources.summary(baseline_model, feature, baseline_tag))

    # Load the focus data
    focus = Summary.Summary(Resources.summary(model, feature, tag))

    # Get the AICC difference
    daicc = focus.aicc - baseline.aicc

    # Determine a filter
    if xaxis == 's+':
        select = np.where(focus.selection > 0)
        plt.semilogx(focus.selection[select], daicc[select], label=label)
    elif xaxis == 's-':
        select = np.where(focus.selection < 0)
        plt.semilogx(-focus.selection[select], daicc[select], label=label)
    elif xaxis == 'R:':
        unphysical = np.where(focus.memory_size<1)[0]
        physical = np.where(focus.memory_size>=1)[0]
        if len(unphysical) > 0:
            # Take the last 'unphysical' point and add it to the start of 'physical'
            physical = np.concatenate((unphysical[-1:],physical))
        p = plt.semilogx(focus.interaction_rate[physical], daicc[physical], '-', label=label)
        if len(unphysical) > 1:
            # Add the unphysical region
            plt.semilogx(focus.interaction_rate[unphysical], daicc[unphysical], ':', c=p[-1].get_color())
    else:
        raise ValueError('Unknown xaxis selection')

def infline(feature, model='wrightfisher', baseline_model='poisson', baseline_tag='baseline'):
    """
    Plot the aicc difference at s=infinity.
    """
    # Load the baseline data
    baseline = Summary.Summary(Resources.summary(baseline_model, feature, baseline_tag))

    # Load the focus data
    focus = Summary.Summary(Resources.summary(model, feature, 'sinf'))

    plt.axhline(focus.aicc - baseline.aicc)


def overdispersion(feature, tag, xaxis='s+', model='wrightfisher', label=None, logbase=None):
    """
    Plot the binary overdispersion for the specified model.
    xaxis can be one of 's+' (positive selection values) or 's-' (negative selection values).
    Lines can be optionally labelled, and the base of the logarithm can be specified (natural
    by default).
    """

    # Load the focus data
    focus = Summary.Summary(Resources.summary(model, feature, tag))

    # The obverdisperson comes as a natural log; we can specify a different base (e.g., 10) with logbase
    scale = 1.0 if logbase is None else np.log(logbase)

    # Determine a filter
    if xaxis == 's+':
        select = np.where((focus.selection > 0) & (focus.log_binary_overdispersion < np.float_('inf')))
        plt.semilogx(focus.selection[select], focus.log_binary_overdispersion[select]/scale, label=label)
    elif xaxis == 's-':
        select = np.where((focus.selection < 0) & (focus.log_binary_overdispersion < np.float_('inf')))
        plt.semilogx(-focus.selection[select], focus.log_binary_overdispersion[select]/scale, label=label)
    else:
        raise ValueError('Unknown xaxis selection')
