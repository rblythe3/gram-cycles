#
# Build a table of all the asymptotes and see how we do
#

import glob, re
import numpy as np, scipy.optimize as spo, scipy.special as sps
import matplotlib.pyplot as plt
from OriginFixation import Resources, Analysis

WF = Analysis.WFBase() # Obtain a single WF engine

def getparm(fn, p):
    mat = re.search('{p}(\d+(?:e[+-]?\d+)?)'.format(p=p), fn)
    return np.float_(mat.group(1)) if mat is not None else None

def mkplot(fitexp=False):

    # Build data sets
    Ns = []
    mus = []
    ss = []
    cis = []
    ays = []

    for fn in glob.glob( Resources.wrightfisher(N='*', mu='*', s='*', mutate_twice=True, target=1) ):
        ts, ps = np.loadtxt(fn,unpack=True)

        # Extract parameter values from filename
        N, mu, s = [ getparm(fn, x) for x in ['N', 'mu', 's'] ]

        # Get Wright-Fisher parameters
        alpha, beta, omega = WF.abo(N, mu, s, 1, 1, float('inf'))
        if beta < omega: continue
        amp = (beta/(beta-omega))**alpha

        # Get the asymptote
        fit = lambda t, a: a * (sps.gammainc( alpha, beta*t ) - np.exp(-omega*t) * amp * sps.gammainc( alpha, (beta-omega)*t ) )
        asymp = spo.curve_fit( fit, ts, ps )[0][0]

        print(N, mu, s, alpha, beta, omega, omega*alpha/beta, asymp)

        Ns += [N]
        mus += [mu]
        ss += [s]
        cis += [omega*alpha/beta]
        ays += [asymp]

    Ns, mus, ss, cis, ays = map(np.array, (Ns, mus, ss, cis, ays))

    xs = np.linspace(min(cis), max(cis))

    for s in np.unique(ss):
        ks = np.where(ss == s)
        plt.plot(cis[ks], ays[ks], 'o', label=str(s))

        if fitexp:
            efit = np.linalg.lstsq( np.vstack( (-cis[ks],) ).T, np.log(ays[ks]) )[0]
            plt.plot(xs, np.exp(-efit*xs))
            print(s, efit[0])


if __name__ == '__main__':

    mkplot(fitexp=True)

    plt.legend()
    plt.show()
