#!/usr/bin/python3
"""
Produce the figure that shows how well a 'usage-based' learning process across
the lifespan fits the historical grammaticalisation data.
This is Figure 6 in the main text.
"""

import matplotlib.pyplot as plt, argparse
from matplotlib import gridspec
from OriginFixation import Resources
from Presentation import PlotLikelihood

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--fig', nargs='?', const='Fig6.eps', help='Output figure to file [off]')
args = parser.parse_args()

plt.figure(6,figsize=(6,4))

gs = gridspec.GridSpec(2,2, height_ratios=[1,1.8], right=0.95, top=0.95, bottom=0.15, wspace=0.05, hspace=0.05)

# Upper left, AICCs for definite
plt.subplot(gs[0])
plt.text(0.90,0.85,'a',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('definite','usage-25yr','R:',label='25yr')
PlotLikelihood.aicc('definite','usage-1yr','R:',label='1yr')
PlotLikelihood.aicc('definite','usage-1month','R:',label='1mth')
PlotLikelihood.aicc('definite','usage-1week','R:',label='1wk')
PlotLikelihood.aicc('definite','usage-1day','R:',label='1dy')
PlotLikelihood.aicc('definite','usage-1hour','R:', label='1hr')
plt.gca().tick_params(labelbottom=False)
plt.ylabel('$\Delta$AICc')
plt.ylim((0,3000))


# Upper right, AICC for indefinite
plt.subplot(gs[1])
plt.text(0.05,0.85,'b',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('indefinite','usage-25yr','R:')
PlotLikelihood.aicc('indefinite','usage-1yr','R:')
PlotLikelihood.aicc('indefinite','usage-1month','R:')
PlotLikelihood.aicc('indefinite','usage-1week','R:')
PlotLikelihood.aicc('indefinite','usage-1day','R:')
PlotLikelihood.aicc('indefinite','usage-1hour','R:')
plt.gca().tick_params(labelbottom=False,labelleft=False)
plt.ylim((0,3000))


# Lower left, AICCs for definite (close-up)
plt.subplot(gs[2])
plt.text(0.90,0.92,'c',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('definite','usage-1hour','R:', label='1hr')
PlotLikelihood.aicc('definite','usage-50min','R:', label='50mins')
PlotLikelihood.aicc('definite','usage-40min','R:', label='40mins')
PlotLikelihood.aicc('definite','usage-30min','R:', label='30mins')
PlotLikelihood.aicc('definite','usage-20min','R:')
PlotLikelihood.aicc('definite','usage-10min','R:')
PlotLikelihood.aicc('definite','usage-1min','R:')
PlotLikelihood.infline('definite')
plt.ylim((0,225))
plt.axhspan(0.0, 10.0, color='0.8') # Plausible region
plt.ylabel('$\Delta$AICc')
plt.xlabel('Interaction rate, R (yr$\mathdefault{{}^{-1}}$)')
plt.legend()


# Lower right, AICC for indefinite (close-up)
plt.subplot(gs[3])
plt.text(0.05,0.92,'d',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('indefinite','usage-1hour','R:')
PlotLikelihood.aicc('indefinite','usage-50min','R:')
PlotLikelihood.aicc('indefinite','usage-40min','R:')
PlotLikelihood.aicc('indefinite','usage-30min','R:')
PlotLikelihood.aicc('indefinite','usage-20min','R:', label='20mins')
PlotLikelihood.aicc('indefinite','usage-10min','R:', label='10mins')
PlotLikelihood.aicc('indefinite','usage-1min','R:', label='1min')
plt.ylim((0,225))
PlotLikelihood.infline('indefinite')
plt.axhspan(0.0, 10.0, color='0.8') # Plausible region
plt.xlabel('Interaction rate, R (yr$\mathdefault{{}^{-1}}$)')
plt.legend(loc=6)
plt.gca().tick_params(labelleft=False)

if args.fig:
    plt.savefig(Resources.figure(args.fig))
    print('Output written in {where}'.format(where=Resources.figure(args.fig)))
else:
    plt.show()
