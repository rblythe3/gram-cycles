#!/usr/bin/python3
"""
Produce the figure that shows how well a one-shot learning process fits the
historical grammaticalisation data.
This is Figure 5 in the main text.
"""

import matplotlib.pyplot as plt, argparse
from matplotlib import gridspec
from OriginFixation import Resources
from Presentation import PlotLikelihood

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--fig', nargs='?', const='Fig5.eps', help='Output figure to file [off]')
args = parser.parse_args()

plt.figure(5,figsize=(6,3))

gs = gridspec.GridSpec(2,3, right=0.92, top=0.95, bottom=0.15, width_ratios=[1.2,2,0.8], height_ratios=[1.2,1], wspace=0.05, hspace=0.05)

# Top left, AICC for s<0
plt.subplot(gs[0])
plt.text(0.025,0.89,'a',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('definite','oneshot-mf','s-', label='definite')
PlotLikelihood.aicc('indefinite', 'oneshot-mf', 's-', label='indefinite')
plt.ylabel('$\Delta$AICc')
plt.gca().invert_xaxis()
plt.ylim((0,20000))
plt.gca().tick_params(labelbottom=False)
plt.legend(loc='upper right')


# Top middle, AICC for s>0
plt.subplot(gs[1])
plt.text(0.025,0.89,'b',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('definite','oneshot-mf','s+', label='definite')
PlotLikelihood.aicc('indefinite','oneshot-mf','s+', label='indefinite')
plt.ylim((0,20000))
plt.gca().tick_params(labelbottom=False,labelleft=False)

# Top right, zoomed in AICC for s>0
plt.subplot(gs[2])
plt.text(0.85,0.89,'c',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('definite','oneshot-mf','s+', label='definite')
PlotLikelihood.aicc('indefinite','oneshot-mf','s+', label='indefinite')
plt.axhspan(0.0, 10.0, color='0.8') # Plausible region
plt.xlim((0.5,100))
plt.ylim((0,300))
plt.gca().tick_params(labelbottom=False,labelleft=False,labelright=True,left=False,right=True)

# Bottom left, overdispersion for s<0
plt.subplot(gs[3])
plt.text(0.025,0.85,'d',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.overdispersion('definite','oneshot-mf','s-',logbase=10)
PlotLikelihood.overdispersion('indefinite','oneshot-mf','s-',logbase=10)
plt.gca().invert_xaxis()
plt.ylabel('Overdispersion')
plt.xlabel('Selection, -s')
plt.ylim((0,280))

plt.gca().set_yticklabels( [ '$\mathdefault{10^{'+str(int(x))+'}}$' for x in plt.gca().get_yticks().tolist() ] )

# Bottom middle, overdispersion for s>0
plt.subplot(gs[4])
plt.text(0.025,0.85,'e',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.overdispersion('definite','oneshot-mf','s+',logbase=10)
PlotLikelihood.overdispersion('indefinite','oneshot-mf','s+',logbase=10)
plt.xlabel('Selection, s')
plt.ylim((0,280))
plt.gca().tick_params(labelleft=False)

# Top right, zoomed in overdispersion for s>0
plt.subplot(gs[5])
plt.text(0.85,0.85,'f',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.overdispersion('definite','oneshot-mf','s+',logbase=10)
PlotLikelihood.overdispersion('indefinite','oneshot-mf','s+',logbase=10)
plt.xlabel('Selection, s')
plt.xlim((0.5,100))
plt.ylim((1,7))
plt.gca().tick_params(labelleft=False,labelright=True,left=False,right=True)

plt.gca().set_yticklabels( [ '$\mathdefault{10^{'+str(int(x))+'}}$' for x in plt.gca().get_yticks().tolist() ] )


if args.fig:
    plt.savefig(Resources.figure(args.fig))
    print('Output written in {where}'.format(where=Resources.figure(args.fig)))
else:
    plt.show()
