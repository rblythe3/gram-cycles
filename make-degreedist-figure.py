#!/usr/bin/env python3

"""
Illustrate the structure of networks with different degree exponents.
This is Figure 4 in the main text
"""

import igraph, numpy as np, argparse
import matplotlib.pyplot as plt
from matplotlib import gridspec
from OriginFixation import Resources

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--seed', type=int, default=42, help='Seed for random graph structure [42]')
parser.add_argument('--fig', nargs='?', const='Fig4.eps', help='Output figure to file [off]')
args = parser.parse_args()

np.random.seed(args.seed)

def sample_graph(N, kmin, kmax, nu, attempts=100):

    # Construct degree distribution
    ks = np.arange(kmin,kmax+1)
    pk = ks**(-(1+nu))
    pk /= np.sum(pk)

    for attempt in range(attempts):
        try:
            return igraph.Graph.Degree_Sequence(list(np.random.choice(ks, N, p=pk)), method='vl')
        except:
            pass

N = 30 # Number of nodes
kmin = 4 # Minimum degree
kmax = 100 # Maximum degree

fig = plt.figure(4, figsize=(6,3))
gs = gridspec.GridSpec(1,2,left=0.05,right=0.95,wspace=0.05)

for n,nu in enumerate([5.6,1.2]):
    plt.subplot(gs[n])

    G = sample_graph(N, kmin, kmax, nu)
    if G is None:
        continue

    L = G.layout_fruchterman_reingold()

    xs, ys = zip(*L.coords)
    for edge in G.es:
        plt.plot([xs[edge.source],xs[edge.target]],[ys[edge.source],ys[edge.target]],c='k',lw=0.4)
    plt.plot(xs,ys,'o',ms=9,alpha=0.8)

    plt.gca().set_xticks([])
    plt.gca().set_yticks([])
    # plt.text(0.04,0.9,chr(ord('a')+n),transform=plt.gca().transAxes,weight='bold')
    plt.text(0.04,0.9,fr'$\nu={nu}$',transform=plt.gca().transAxes,weight='bold')


if args.fig:
    plt.savefig(Resources.figure(args.fig))
    print('Output written in {where}'.format(where=Resources.figure(args.fig)))
else:
    plt.show()
