#!/bin/bash
#
# Run this script to generate all the data that are needed for Fig S1.4 in the SI

dryrun=0
clobber=0

for arg in "$@"; do
  case "$arg" in
    --dryrun)
      dryrun=1
      ;;
    --clobber)
      clobber=1
      ;;
    --help)
      echo "Generate a representative set of jump moments for the demonstration model"
      echo "Options:"
      echo "  --dryrun - show which commands would be run"
      echo "  --clobber - allow overwrite of existing data files"
      exit
      ;;
  *)
    echo "Usage: get-demo-model-data.sh [--dryrun] [--clobber] [--help]"
    exit
  esac
done

# runbatch <r> <R> <i>
# runs a batch with mean bias r, standard deviation R, and initial frequency i
runbatch () {
  cmd="./demo-model -r$1 -R$2 -i$3 -m10000"
  for run in {1..100}; do
    ofn="Output/Demo/r$1-R$2-run$run.dat"
    echo -n "$(date): $cmd > $ofn; "
    if [ $dryrun -eq 1 ]; then
      if [ $clobber -eq 0 ] && [ -e "$ofn" ]; then
        echo "output file exists: would skip"
      else
        echo "would run"
      fi
    else
      if [ $clobber -eq 0 ] && [ -e "$ofn" ]; then
        echo "output file exists: skipping"
      else
        echo "running"
        $cmd > $ofn
      fi
    fi
  done
}

runbatch 0 0.005 0.5
runbatch 0.005 0 0.1
runbatch 0.005 0.005 0.1
