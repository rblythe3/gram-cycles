#!/usr/bin/env python3

"""
Compare estimated and numerically-determined jump moments in the demonstration model.
This is Figure 4 in the appendix
"""

import argparse, numpy as np, matplotlib.pyplot as plt
from scipy.stats import gamma
from scipy.integrate import quad
from OriginFixation import Resources

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--fig', nargs='?', const='FigS1_4.pdf', help='Output figure to file [off]')
args = parser.parse_args()

# Relevant parameters
N             = 1000 # Mean population size
mu_death      = 60.0 # Mean age of death
sig_death     = 12.0 # Standard deviation in age of death
mu_rate0      =  1.0 # Mean initial interaction rate
sig_rate0     =  0.1 # Standard deviation in the initial interaction rate
mu_ratio      = 10.0 # Mean ratio between the initial and asymptotic interaction rate
sig_ratio     = 10.0 # Standard deviation in the initial and asymptotic interaction rate
mu_decaytime  = 20.0 # Mean decay time for interaction rate
sig_decaytime = 10.0 # Standard deviation in decay time for interaction rate
mu_replace    = 0.15 # Mean fraction of store replaced
sig_replace   = 0.1  # Standard deviation in fraction of store replaced

delta_t       = 10 # Time period over which jump moments are estimated

## Analtyical estimates

# Parameters of gamma distribution t^(alpha-1) * exp(-beta*t) in age of death
alpha_death = (mu_death/sig_death)**2
beta_death = mu_death/sig_death**2

# Calculate average of 1/(x+1) wrt to gamma dist
rpdf = gamma( (mu_ratio/sig_ratio)**2, scale=sig_ratio**2/mu_ratio ).pdf
ravg = quad(lambda x: rpdf(x)/(1+x), 0, np.inf)[0]

# Calculate average of decay time part wrt to gamma dist
tpdf = gamma( (mu_decaytime/sig_decaytime)**2, scale=sig_decaytime**2/mu_decaytime ).pdf
tavg = quad(lambda x: tpdf(x) * x * (beta_death*x/(1+beta_death*x))**(alpha_death-1), 0, np.inf)[0]

# Mean rate at which interactions occur
R = mu_rate0*ravg + mu_rate0 * (1.0-ravg) * beta_death / (alpha_death-1) * (mu_decaytime - tavg)

def amplitudes(mu_bias,sig_bias):
    "Analtyical estimate of first and second jump moment amplitudes"
    return R*float(mu_bias)*mu_replace*delta_t, R*(mu_replace**2+sig_replace**2)*delta_t/N

## Numerical data and plots
fig, ax = plt.subplots(2,3,sharex='all',sharey='row',figsize=(10,6))
xs = np.linspace(0,1)
for col, (mu_bias, sig_bias) in enumerate( (('0','0.005'), ('0.005','0'), ('0.005','0.005')) ):
    # Sum counts across bins in different data files
    bs, ns, ms, vs = (None,)*4
    for fn in Resources.demomodel(mu_bias, sig_bias):
        b, n, m, v = np.loadtxt(fn, unpack=True)
        if bs is None:
            bs, ns, ms, vs = b, n, m, v
        else:
            ns += n
            ms += m
            vs += v

    # Shift to mid-point of each bin
    bs = ( bs + np.concatenate( (bs[1:], [1]) ) )/2
    # Retain only bins with positive counts and obtain averages
    bs = bs[ns>0]
    ms = ms[ns>0]/ns[ns>0]
    vs = vs[ns>0]/ns[ns>0]

    # Get theoretical amplitudes
    t1a, t2a = amplitudes(mu_bias,sig_bias)

    # Get empirical fit as well
    e1a = np.linalg.lstsq( np.array([bs*(1-bs)]).T, ms, rcond=None )[0][0]
    e2a = np.linalg.lstsq( np.array([bs*(1-bs)]).T, vs, rcond=None )[0][0]

    # Upper row: mean
    ax[0,col].plot(bs, ms, 'o', ms=5, alpha=0.5, label='simulation')
    ax[0,col].plot(xs, t1a*xs*(1-xs), '-', lw=2, label='analytical')
    ax[0,col].plot(xs, e1a*xs*(1-xs), ':', lw=2, label='empirical')

    # Lower row: variance
    ax[1,col].plot(bs, vs, 'o', ms=5, alpha=0.5, label='simulation')
    ax[1,col].plot(xs, t2a*xs*(1-xs), '-', lw=2, label='analytical')
    ax[1,col].plot(xs, e2a*xs*(1-xs), ':', lw=2, label='empirical')

    ax[1,col].set_xlabel(r'frequency, $x$')

ax[0,0].set_ylabel(r'first jump moment')
ax[1,0].set_ylabel(r'second jump moment')

ax[0,0].legend()

if args.fig:
    plt.savefig(Resources.figure(args.fig))
    print('Output written in {where}'.format(where=Resources.figure(args.fig)))
else:
    plt.show()
