#!/usr/bin/python3
"""
Generate the table summarising the community compositions.
"""

import argparse
from OriginFixation import Resources
from Presentation import LongTable, Format

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--split', '-s', default=4, type=int, help='Maximum components per line [4]')
args = parser.parse_args()

# Reload the compositions
compositions = Resources.retrieve(Resources.compositions())

# Now create the table writer
table = LongTable.LongTable(Resources.table('commcomps'),
    ['Language', 'Geographical composition'],
    caption = r"""
Geographical composition of the speech community for each language in the sample.
The resulting relative historical average population sizes are given in
Table~\ref{stab:histories}.
""",
    label = 'stab:commcomps'
)

def as_sum(pclocs):
    "Convert list of (percentage,location) into sum representation"
    return ' + '.join( [ pc+r'\% '+Format.decamel(loc) for pc,loc in pclocs ] )

for language in sorted(compositions):
    if len(compositions[language])<=args.split:
        table.row( [ Format.decamel(language), as_sum(compositions[language]) ] )
    else:
        # split compositions into chunks of args.split
        chunks = [ compositions[language][start:start+args.split] for start in range(0,len(compositions[language]),args.split) ]
        table.splitrows( *(
            [ [ Format.decamel(language), as_sum(chunks[0]) ] ] +
            [ [ r'\hspace{1em} + ' + as_sum(chunk) ] for chunk in chunks[1:] ]
        ) )

table.finalise()
print('Output written in {where}'.format(where=Resources.table('commcomps')))
