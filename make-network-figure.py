#!/usr/bin/python3
"""
Produce the figure that shows how well cultural evolutionary models
on heterogeneous networks fit the historical grammaticalisation data.
This is Figure 7 in the main text.
"""

import matplotlib.pyplot as plt, argparse
from matplotlib import gridspec
from OriginFixation import Resources
from Presentation import PlotLikelihood

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--fig', nargs='?', const='Fig7.eps', help='Output figure to file [off]')
args = parser.parse_args()

plt.figure(7, figsize=(6,4))

#gs = gridspec.GridSpec(2,2, right=0.95, top=0.95, height_ratios=[1,1.25], wspace=0.05, hspace=0.05)
gs = gridspec.GridSpec(3,2, right=0.95, top=0.95, height_ratios=[0.65,0.55,1], wspace=0.05, hspace=0.05)

# Top left, AICC for definite, various nu
plt.subplot(gs[0])
plt.text(0.90,0.85,'a',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('definite','oneshot-n1.3','s+')
PlotLikelihood.aicc('definite','oneshot-n1.2','s+')
PlotLikelihood.aicc('definite','oneshot-n1.1','s+')
PlotLikelihood.infline('definite')
# plt.axhspan(0.0, 5.0, color='0.8') # Plausible region
plt.ylabel('$\Delta$AICc')
plt.ylim((0,4200))
plt.gca().tick_params(labelbottom=False)

# Top right, AICC for indefinite, various nu
plt.subplot(gs[1])
plt.text(0.05,0.85,'b',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('indefinite','oneshot-n1.3','s+',label=r'$\nu$=1.3')
PlotLikelihood.aicc('indefinite','oneshot-n1.2','s+',label=r'$\nu$=1.2')
PlotLikelihood.aicc('indefinite','oneshot-n1.1','s+',label=r'$\nu$=1.1')
PlotLikelihood.infline('indefinite')
# plt.axhspan(0.0, 5.0, color='0.8') # Plausible region
plt.ylim((0,4200))
plt.gca().tick_params(labelbottom=False,labelleft=False)
plt.legend()
# plt.legend(loc=1,fontsize='medium')

plt.subplot(gs[2])
plt.text(0.90,0.82,'c',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('definite','oneshot-n1.3','s+')
PlotLikelihood.aicc('definite','oneshot-n1.2','s+')
PlotLikelihood.aicc('definite','oneshot-n1.1','s+')
PlotLikelihood.infline('definite')
plt.axhspan(10.0, 20.0, color='0.9') # Less plausible region
plt.axhspan(0.0, 10.0, color='0.8') # Plausible region
plt.ylabel('$\Delta$AICc')
plt.ylim((0,110))
plt.gca().minorticks_on()
plt.gca().tick_params(labelbottom=False)
plt.gca().tick_params(axis='x',which='minor',bottom=False)

plt.subplot(gs[3])
plt.text(0.05,0.82,'d',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('indefinite','oneshot-n1.3','s+')
PlotLikelihood.aicc('indefinite','oneshot-n1.2','s+')
PlotLikelihood.aicc('indefinite','oneshot-n1.1','s+')
PlotLikelihood.infline('indefinite')
plt.axhspan(10.0, 20.0, color='0.9') # Less plausible region
plt.axhspan(0.0, 10.0, color='0.8') # Plausible region
plt.ylim((0,110))
plt.gca().minorticks_on()
plt.gca().tick_params(labelbottom=False,labelleft=False)
plt.gca().tick_params(axis='x',which='minor',bottom=False)

# Bottom left, AICC for definite, fixed nu, various Tm
plt.subplot(gs[4])
plt.text(0.90,0.075,'e',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('definite','oneshot-n1.2','s+')
PlotLikelihood.aicc('definite','categorical-1month','s+')
PlotLikelihood.aicc('definite','categorical-1day','s+')
PlotLikelihood.aicc('definite','categorical-1hr','s+')
PlotLikelihood.infline('definite')
plt.axhspan(10.0, 20.0, color='0.9') # Less plausible region
plt.axhspan(0.0, 10.0, color='0.8') # Plausible region
plt.xlabel('Selection, s')
plt.ylabel('$\Delta$AICc')
plt.ylim((0,225))

# Bottom right, AICC for indefinite, fixed nu, various Tm
plt.subplot(gs[5])
plt.text(0.05,0.075,'f',transform=plt.gca().transAxes,weight='bold')
PlotLikelihood.aicc('indefinite','oneshot-n1.2','s+',label='25yr')
PlotLikelihood.aicc('indefinite','categorical-1month','s+',label='1month')
PlotLikelihood.aicc('indefinite','categorical-1day','s+',label='1day')
PlotLikelihood.aicc('indefinite','categorical-1hr','s+',label='1hr')
PlotLikelihood.infline('indefinite')
plt.axhspan(10.0, 20.0, color='0.9') # Less plausible region
plt.axhspan(0.0, 10.0, color='0.8') # Plausible region
plt.xlabel('Selection, s')
plt.ylim((0,225))
plt.gca().tick_params(labelleft=False)
plt.legend()

if args.fig:
    plt.savefig(Resources.figure(args.fig))
    print('Output written in {where}'.format(where=Resources.figure(args.fig)))
else:
    plt.show()
