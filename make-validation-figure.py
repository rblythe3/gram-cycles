#!/usr/bin/python3
"""
Produce the figure that demonstrates the calibration of the Origin-Fixation
model using exact numerical computations for the Wright-Fisher model.
This is Figure 3 in the appendix.
"""

import matplotlib.pyplot as plt, numpy as np, argparse
from matplotlib import gridspec
from OriginFixation import Resources
from Presentation import PlotWfbme, PlotAsymptotes

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--fig', nargs='?', const='FigS1_3.pdf', help='Output figure to file [off]')
args = parser.parse_args()

plt.figure(1)

gs = gridspec.GridSpec(2,2, left=0.1, bottom=0.1, right=0.95, top=0.95, wspace=0.25, hspace=0.3)

# Upper left, P(t) for drift conditioned on fixing, so asymptote will be 1
plt.subplot(gs[0])
PlotWfbme.mkplot('100', '1e-3', '0', cutoff=4000, points=20)
# plt.locator_params(nbins=4,axis='x')
# plt.gca().tick_params(labelbottom=False)
plt.xlim((0,4000))
plt.xlabel('time, t (generations)')
plt.ylabel('P(t)')

plt.legend(loc=4,fontsize='medium')


# Upper right, P(t) with selection, not conditioned on fixing, so asymptote will be not 1
plt.subplot(gs[1])
PlotWfbme.mkplot('200', '5e-4', '1e-3', twice=True, cutoff=4000, points=20)
# plt.locator_params(nbins=4,axis='x')
# plt.gca().tick_params(labelbottom=False,labelleft=False)
plt.xlim((0,4000))
plt.xlabel('time, t (generations)')
plt.ylabel('P(t)')

# Lower left, P(t) for a double mutant, conditioned on fixing
plt.subplot(gs[2])
PlotWfbme.mkplot('100', '1e-3', '1e-2', twice=True, target=2, cutoff=4000, points=20)
# plt.locator_params(nbins=4,axis='x')
plt.xlim((0,4000))
plt.xlabel('time, t (generations)')
plt.ylabel('P(t)')


# Lower right, Asymptotes as a function of the amount of interference
plt.subplot(gs[3])
PlotAsymptotes.mkplot()
plt.xlim((0,8))
xs = np.linspace(0,8)
plt.plot(xs, np.exp(-xs), 'r-', lw=2)
plt.ylabel(r'P($\mathdefault{\infty}$)')
plt.xlabel(r'Interference, $\bar{\omega} T_P$')
# plt.gca().tick_params(labelleft=False)
# plt.legend(loc=3,fontsize='medium')

if args.fig:
    plt.savefig(Resources.figure(args.fig))
    print('Output written in {where}'.format(where=Resources.figure(args.fig)))
else:
    plt.show()
