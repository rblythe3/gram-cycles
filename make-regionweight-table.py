#!/usr/bin/python3

"""
Generate the table summarising the regional weights.
"""

import argparse
from OriginFixation import Resources
from Presentation import LongTable, Format

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--columns', '-c', default=2, type=int, help='Number of columns')
args = parser.parse_args()

# Reload the population fits
weights = Resources.retrieve(Resources.weightfit())

columns = args.columns

# Now create the table writer
table = LongTable.LongTable(Resources.table('regwts'),
    ['Region', 'Weight']*columns,
    caption = """
Relative historical average size of each geographical region relevant to the
languages in the sample (all to 3~s.f.). These sizes are normalized such that
the smallest such region (Iceland) has a relative size of 1.
    """,
    label = 'stab:regwts',
    vruleskip=2
)

locations = sorted(weights.keys())
rows = (len(weights) + (columns-1)) // columns
locations += [None]*(rows*columns - len(locations))

for row in range(rows):

    cells = []
    for col in range(columns):
        location = locations[rows*col + row]
        if location is not None:
            cells += [ Format.decamel(location), Format.to_precision(weights[location],3) ]

    table.row(cells)

table.finalise()
print('Output written in {where}'.format(where=Resources.table('regwts')))
