#!/usr/bin/python3
"""
Generate the summary output required to produce the figures in the paper
by running the likelihood.py script for various parameter combinations.
"""

import argparse, os.path, subprocess
from OriginFixation import Resources

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--clobber', action='store_true', help='Re-run analysis even when summary output already exists [off]')

args = parser.parse_args()

def mksummary(model, feature, tag, *params):
    # Look to see if summary file already exists
    print("{m}-{f}-{t}:".format(m=model, f=feature, t=tag),end=None)
    if not args.clobber and os.path.exists(Resources.summary(model, feature, tag)):
        print("summary exists, skipping")
        return
    print("running parameter combinations "+(' '.join(params)))
    cmd = ['./likelihood.py', model, '-F{f}'.format(f=feature), '-S{t}'.format(t=tag)] + list(params)
    # print(cmd)
    subprocess.call(cmd)

def mksummaries(model, tag, *params):
    mksummary(model, 'definite', tag, *params)
    mksummary(model, 'indefinite', tag, *params)

# Poisson baseline
mksummaries('poisson', 'baseline')

# Wright-Fisher infinite selection limit (universal)
mksummaries('wrightfisher', 'sinf', '-sinf')

# One-shot (mean-field)
mksummaries('wrightfisher', 'oneshot-mf', '-s-3e-6:-1e-9:log80,1e-9:1e3:log80')

# One-shot (networks)
for net_exponent in ['1.1','1.2','1.3']:
    mksummaries('wrightfisher', 'oneshot-n{n}'.format(n=net_exponent), '-n{n}'.format(n=net_exponent), '-s1e-9:1e5:log150')

# Usage-based model
minute = 1.0/365.25/24.0/60.0 # Fraction of a year corresponding to a minute

# Fraction of a year that corresponds to various memory times
memtimes = [
    ('25yr', 0.04), ('10yr', 0.1), ('1yr', 1.0),
    ('1month', 1.0/12.0), ('1week', 7.0/365.25), ('1day', 1.0/365.25),
    ('1hour', 60.0*minute), ('50min', 50.0*minute), ('40min',40*minute),
    ('30min', 30.0*minute), ('20min', 20.0*minute), ('10min',10*minute),
    ('1min', minute)
]

# Mean field, s=0, range of R, K fixed implicity
for tag, memtime in memtimes:
    mksummaries('wrightfisher', 'usage-{t}'.format(t=tag), '-m{memtime}'.format(memtime=memtime), '-R1:1e7:log50' )

# Update frequencies with K=1 that correspond to specific memory lifetimes
updatefreqs = [
    ('1month', 12.0), ('1day', 365.25), ('1hr', 365.25*24.0)
]

# Network, nu=1.2, range of s, K=1, various R
for tag, freq in updatefreqs:
    mksummaries('wrightfisher', 'categorical-{t}'.format(t=tag), '-s1e-9:1e5:log150', '-n1.2', '-R{freq}'.format(freq=freq))
