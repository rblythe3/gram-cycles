#!/usr/bin/python3

"""
Test independence of stationary frequencies and numbers of changes
between a pair of features
"""

import argparse, subprocess, re, os
import numpy as np, scipy.stats as sps
from OriginFixation import Resources

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--mcsamples', '-s', type=int, default=10000000, help='Number of Monte Carlo samples for p-value estimation [10M; 0=disable MC]')
parser.add_argument('--mcburnin', '-b', type=int, default=1000000, help='Number of Monte Carlo burn-in steps [1M]')
parser.add_argument('--mcdelta', '-d', type=int, default=10, help='Number of Monte Carlo steps between samples [10]')
parser.add_argument('--seed', '-S', help='Monte Carlo random number seed(s) [generate automatically]')
parser.add_argument('features', nargs='*', default=['definite','indefinite'])

args = parser.parse_args()

if len(args.features) != 2:
    print("Must supply exactly two features to test")
    exit(1)

if args.seed is not None:
    seeds = [int(x) for x in args.seed.split(',') ]
else:
    seeds = []

def make_seed():
    """
    Generate a random number seed. Returns None if was not able to generate automatically
    """
    global seeds
    if args.seed is None:
        try:
            # Attempt to get a seed from the system random device
            return int.from_bytes(os.urandom(4),'little')
        except:
            # Revert to default
            return None
    # Work through the seeds, and keep using the last one if we run out
    if len(seeds) > 1:
        seed = seeds.pop(0)
        return seed
    elif len(seeds) == 1:
        return seeds[0]


def test_table(t, name=None):
    """
    Apply the chi-sq test and MC test to the table t
    """

    if name is not None:
        print("Testing {} for independence".format(name))

    print("Contingency table:")
    print(t)

    chi2, p, df, exp = sps.chi2_contingency(t)
    print("Chi-squared test (null=independent): p={}".format(p))
    if np.any(t<5) or np.any(exp < 5):
        print("\tWarning: at least one expected or observed count is less than 5, treat the chi-sq p-value with caution")

    if args.mcsamples > 0:
        # Run the MC sampler as a subprocess and extract the pvalue from the output
        seed = make_seed()
        mc = subprocess.run([
            './contingency-pvalue',
            '-r{}'.format(t.shape[0]),
            '-c{}'.format(t.shape[1]),
            '-s{}'.format(args.mcsamples),
            '-b{}'.format(args.mcburnin),
            '-d{}'.format(args.mcdelta),
            ] + ([] if seed is None else ['-S{}'.format(seed)]) + list(t.flatten().astype(str)),
             capture_output=True)
        if mc.returncode == 0:
            mat = re.search(b'p-value.*?(\d+)\s+samples.*:\s*(.*)\n', mc.stdout)
            if mat is None:
                print("Warning: Unexpected output from Monte Carlo sampler")
            else:
                print("Monte Carlo test ({} samples, seed={}; null=independent): p={}".format(
                    mat.group(1).decode('ascii'),
                    'default' if seed is None else seed,
                    mat.group(2).decode('ascii'))
                )
        else:
            print("Warning: Monte Carlo sampling did not complete successfully")


# Independence of stationary distribution
try:
    joint = Resources.retrieve(Resources.stationary('-vs-'.join(args.features)))
except:
    joint = Resources.retrieve(Resources.stationary('-vs-'.join(reversed(args.features))))

test_table(joint, 'joint stationary distribution')

# Independence of number of changes. We need to collate the number of changes
# into a table.

first, second = ( Resources.retrieve(Resources.languagechanges(f)) for f in args.features )

# Convert to a dict mapping language -> number of states
first = dict( (x.language, x.states) for x in first )
second = dict( (x.language, x.states) for x in second )

# Get the common set of languages
languages = set(first.keys()).intersection(second.keys())

# Get distinct row and column values
rows = sorted(set(first.values()))
cols = sorted(set(second.values()))

# Convert to a mapping to a zero-based index
rows = dict( (r,n) for n,r in enumerate(rows))
cols = dict( (c,n) for n,c in enumerate(cols))

# Build the contingency table
ctab = np.zeros((len(rows),len(cols)),dtype=np.int)

for l in languages:
    ctab[rows[first[l]],cols[second[l]]] += 1

print()
test_table(ctab, 'number of language changes')
