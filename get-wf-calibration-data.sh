#!/bin/bash
#
# Run this script to generate all the data that are needed to calibrate the WF
# and OF models.
#

passthru=""
for arg in "$@"; do
  case "$arg" in
    --dryrun|--clobber)
      passthru="$passthru $arg"
      ;;
    --help)
      echo "Generate a representative set of transition probabilities for the Wright-Fisher model"
      echo "Options:"
      echo "  --dryrun - show which commands would be run"
      echo "  --clobber - allow overwrite of existing data files"
      exit
      ;;
    *)
      echo "Usage: get-wf-calibration-data.sh [--dryrun] [--clobber] [--help]"
      exit
  esac
done

# Drift with no mutation, conditioned on fixation
./runwf.py -N100 -m0 -s0 -g1000 -d1 -C $passthru

# Drift with moderate selection in a bigger system, conditioned on fixation
./runwf.py -N150 -m0 -s1e-2 -g1000 -d1 -C $passthru

# Drift with a fairly large mutation rate
./runwf.py -N100 -m1e-3 -s0 -g5000 -d1 $passthru
# Reasonably large mutation and selection, with interference
./runwf.py -N200 -m5e-4 -s1e-3 -g20000 -d4 --twice $passthru
# Ever larger selection, with two transitions
./runwf.py -N100 -m1e-3 -s1e-2 -g10000 -d2 --twice --target=2 $passthru

# Now generate a whole host of sets for different N, m and s to determine the effect of interference
for N in 50 100 200; do
  for s in 0 1e-4 1e-3 1e-2 1e-1; do
    for m in 1e-4 2e-4 5e-5 1e-3 2e-3 5e-3 1e-2 2e-2; do
      ./runwf.py -N$N -m$m -s$s -g20000 -d4 --twice $passthru
    done
  done
done
