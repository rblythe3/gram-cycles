#!/usr/bin/python3
"""
Preprocess the language change data in the Input directory. This includes:
    * fitting a population growth model to defined geographical areas
    * forming linear combinations of these areas to make speech communities
    * computing the stationary distribution of the different variants from WALS
    * caching the relevant language change data in readiness for likelihood evaluation
    * archiving the data in a form that can then be used to generate figures and tables for the paper
"""

import argparse, re, numpy as np
from OriginFixation import Resources, DataTable, PopulationCurve, LanguageChange

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--basetime', '-t', default='0', help='Reference point in time [0]')
parser.add_argument('--baseloc', '-l', default='iceland', help='Reference location [iceland]')
parser.add_argument('--degree', '-d', default=4, type=int, help='Degree of polynomial fit to log population size [4]')
parser.add_argument('--clobber', action='store_true', help='Overwrite existing files even if dependencies are unchanged [off]')

args = parser.parse_args()

# Transform applied to location keys
lkeytrans = str.lower

# Read the historical population size data
populations = DataTable.DataTable(Resources.populations(),key='location',keytransform=lkeytrans)

# Check that the base location is in the sample
if args.baseloc not in populations:
    print("Base location '{p}' not defined in populations".format(p=args.baseloc))
    exit(1)

# Our hypothesis is that population n at time t is estimated by N0 w_n c_t
# or more precisely that its log is given by log (N0) + log(w_n) + log(c_t) + noise
# We do a linear fit to the log sizes with N0, w_n and c_t as parameters. Since
# we can shift all w_n and c_t by a common amount and get the same answer we
# force w_n=1 and c_t=1 for the reference population and reference time, so
# must drop these from the data.

flocs = set()  # This contains all but the reference location
ftimes = set() # This contains all but the reference time
datapoints = 0 # Number of datapoints
basetimein = False # Flag to tell us if the reference time is actually recorded

for population in populations:
    lkey = lkeytrans(population['location'])
    if lkey != args.baseloc:
        flocs.add(lkey)
    # Extract numeric keys and interpret as times
    for pt in population.keys():
        if re.match('^\-?\d+$', pt) and population[pt] != '':
            # Skip the base time, but record its presence
            if args.basetime == pt:
                basetimein = True
            else:
                ftimes.add(pt)
            datapoints += 1

if not basetimein:
    print("Base time '{t}' not defined in populations".format(t=args.basetime))
    exit(1)

# Sort the times and locations
flocs = sorted(flocs)
ftimes = sorted(ftimes,key=int)

fparms = len(flocs) + len(ftimes) + 1 # Number of fit parameters (includes N0)
dof = datapoints - fparms # Number of degrees of freedom in the fit

if dof <= 0:
    print("Too few degrees of freedom in the model fit")
    exit(1)

print("Fitting linear model with {fparms} parameters to {datapoints} data points (dof={dof});".format(fparms=fparms,datapoints=datapoints,dof=dof),end=None)

# To perform the fit we need to construct a coefficient matrix where each row
# is a datapoint, and the first len(flocs) columns contain a 1 in the position
# corresponding to the location, and the next len(ftimes) columns contains a 1
# in the position corresponding to the time. The final column always contains a 1
# corresponding to the addition constant N0. The rhs vector contains the log
# of the population size for the given location and time.

coeff = np.zeros((datapoints,fparms))
rhs = np.zeros(datapoints)
fitpoint = 0
for population in populations:
    lkey = lkeytrans(population['location'])
    n = flocs.index(lkey) if lkey in flocs else None
    for pt in population.keys():
        if re.match('^\-?\d+$', pt) and population[pt] != '':
            # Record a 1 in the column corresponding to the location
            if n is not None:
                coeff[fitpoint,n] = 1.0
            # Record a 1 in the column correspond to the time
            if pt in ftimes:
                coeff[fitpoint,len(flocs)+ftimes.index(pt)] = 1.0
            # Record a 1 in the column correspining to the global parameter N0
            coeff[fitpoint,-1] = 1.0
            # Record the log population size in the rhs vector
            rhs[fitpoint] = np.log(np.float_(population[pt]))
            fitpoint += 1

# Perform the least-square fit
fit, resid, rank, s = np.linalg.lstsq(coeff, rhs, rcond=None)

print('R^2={rsq}'.format(rsq=1.0 - np.float_(resid)/np.sum((rhs-np.mean(rhs))**2)))

print("Constructing population curve;",end=None)

# Extract the population scale, i.e., the size of the reference population at
# the reference time. Since the original data are given in millions and then logged,
# we must transform appropriately.
N0 = np.exp(fit[-1])*1e6
print('N0={N0}'.format(N0=N0))

# Extract the weights w_n
weights = dict( zip( [ populations[x]['location'] for x in [args.baseloc] + flocs ], [1.0] + list(np.exp(fit[:len(flocs)])) ) )

# Archive them (if required)
if Resources.store(weights, Resources.weightfit(), depends=None if args.clobber else [Resources.populations()]):
    print('+ Archived weights of geographical population sizes')
else:
    print('- No changes to geographical population sizes, weights not re-archived')

# Reinstate the base time into the time series
at = np.searchsorted(ftimes, args.basetime)
times = np.array(ftimes[:at] + [args.basetime] + ftimes[at:], dtype=np.float_)

# Determine the best-fit log population size across all time points; this involves
# inserting 0.0 at the base time
logsize = np.concatenate( (fit[len(flocs):len(flocs)+at], [0.0], fit[len(flocs)+at:-1]) )

# We now fit a polynomial to this best-fit log population size time series, and
# construct the PopulationCurve object that is used to represent it.
popcurve = PopulationCurve.PopulationCurve( N0, np.polynomial.polynomial.polyfit(times, logsize, args.degree) )

# Archive it (if required)
if Resources.store(popcurve, Resources.populationcurve(), depends=None if args.clobber else [Resources.populations()]):
    print('+ Archived population growth curve')
else:
    print('- No changes to geographical population sizes, population curve not re-archived')

# Create arrays of observed and fit population sizes for each region for visualisation of the fit
# visfit[location] = ( times, observed, normalised, fit )
# where times is the set of times at which observations were made
# observed is the set of absolute observed population sizes
# normalised is divided by the estimated weight (so normalised to the size of the reference population)
# fit is the prediction for the absolute population size from the polynomial fit
visfit = {}
for location in weights:
    population = populations[location]
    loctimes = sorted([ pt for pt in population.keys() if re.match('^\-?\d+$', pt) and population[pt] != '' ], key=int)
    visfit[location] = list(zip(*[ (float(time), float(population[time])*1.0e6, float(population[time])*1.0e6/weights[location], popcurve(weights[location], float(time))) for time in loctimes ]))

# Add to this the master curve: this has no observations. The normalised series is the average
# that emerges from the linear least squares fit; the fit is then the polynomial fit to that average.
visfit['*'] = (times, None, N0*np.exp(logsize), [ popcurve(1.0, float(time)) for time in times ])

# Archive these data (if required)
if Resources.store(visfit, Resources.regionfit(), depends=None if args.clobber else [Resources.populations()]):
    print('+ Archived fits to geographical population sizes')
else:
    print('- No changes to geographical population sizes, fits not re-archived')

# Establish stationary distributions, and archive if required
print()
print("Computing stationary distributions")

for feature in ['definite', 'indefinite']:

    stat = np.loadtxt(Resources.wals(feature))
    stat /= np.sum(stat)

    if Resources.store(stat, Resources.stationary(feature), depends=None if args.clobber else [Resources.wals(feature)]):
        print('+ Archived stationary distribution for {f} article'.format(f=feature))
    else:
        print('- No changes to WALS data for {f} article, distribution not re-archived'.format(f=feature))

# Also archive joint distributions
for combination in [ ('definite','indefinite') ]:
    feature = '-vs-'.join(combination)
    jstat = np.loadtxt(Resources.wals(feature),dtype=int)
    if Resources.store(jstat, Resources.stationary(feature), depends=None if args.clobber else [Resources.wals(feature)]):
        print('+ Archived joint stationary distribution for {c} combination'.format(c=combination))
    else:
        print('- No changes to WALS data for {c} combination, joint distribution not re-archived'.format(c=combination))

# Create a sequence of LanguageChange objects for each article
# This involves estimating the weight of the speech community through linear
# combinations of the underlying geographical regions.

languages = DataTable.DataTable(Resources.languages())

definites = []
indefinites = []
compositions = {}

print()
print("Processing language change datasets")

def split2(x):
    x = x.split(':',1)
    return tuple(x) if len(x) == 2 else (x[0],None)

for language in languages:

    periods = [ (np.float_(s),np.float_(e)) for s,e in [ x.split(':') for x in language['period'].split(',')] ]
    compo = re.findall('([\d.]+)%(\w+)',language['weight'])
    compositions[language['language']] = compo
    weight = np.sum([ (np.float_(pc)/100.0 * weights[loc]) for pc,loc in compo ])
    root = language['parent'] == ''
    dstates = [ int(x) for x in re.findall('D(\d+)',language['definite']) ]
    istates = [ int(x) for x in re.findall('I(\d+)',language['indefinite']) ]
    references = [ split2(x) for x in language['references'].split(';') ]

    definites.append( LanguageChange.LanguageChange(language['language'], weight, periods, dstates[0], len(dstates), root, references) )
    indefinites.append( LanguageChange.LanguageChange(language['language'], weight, periods, istates[0], len(istates), root, references) )


# Archive language compositions (if required)

if Resources.store(compositions, Resources.compositions(), depends=None if args.clobber else [Resources.languages()]):
    print('+ Archived language compositions')
else:
    print('- No changes to geographical population sizes, compositions not re-archived')


# Archive language change data (if required)
if Resources.store(definites, Resources.languagechanges('definite'), depends=None if args.clobber else [Resources.populations(), Resources.languages()]):
    print('+ Archived definite article changes')
else:
    print('- No changes to geographical population sizes or language change data, definite changes not re-archived')

if Resources.store(indefinites, Resources.languagechanges('indefinite'), depends=None if args.clobber else [Resources.populations(), Resources.languages()]):
    print('+ Archived indefinite article changes')
else:
    print('- No changes to geographical population sizes or language change data, indefinite changes not re-archived')


print()
print("Preprocessing complete. If you change the source data, you will need to re-run.")
print("(In any case, use the --clobber option if you want to force a refresh of the archive)")
