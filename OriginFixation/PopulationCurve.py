"""
PopulationCurve represents the growth of a population over time.

It is initialised with a base population size and an array of polynomial coefficients
in order of increasing degree. This is then exponentiated and multiplied by a weight
and the base size to estimate a population size for a given region and time.
"""

import numpy as np

class PopulationCurve(object):

    def __init__(self, N0, poppolyfit):
        """
        N0: base population size.
        poppolyfit: polynomial coefficients (lowest degree first)
        """
        self.N0 = N0
        self.poppolyfit = poppolyfit

    def __call__(self, weight, t):
        """
        Obtain an estimate of a population size for a region with specified
        weight at the specified time t.
        """
        return weight * self.N0 * np.exp( np.polynomial.polynomial.polyval(t, self.poppolyfit) )
