"""
Direct Laplace Inversion of the Poisson and Poisson-Gamma likelihood functions.
Over the full parameter space of interest, we can get widely varying results
from both functions, so we need a few tricks to perform the computation in log
space.

When we expect an exponential decay, we shift the contour to place the dominant
singularity at the origin. This avoids generating an exponential in linear space
that can cause overflow.

Amplitudes of the leading exponential can also get very small, so we estimate
the scale of the largest contibution and subtract that out in log space as well.

The Euler inverter with just five digits of precision seems to be good enough for
our needs, and results compare well with regimes where an exact computation is
possible.
"""

from . import LaplaceInversion
import numpy as np

# Since creation of an inverter may be an expensive operation, we create
# and re-use a default inverter on demand

_default_inverter = None

def _make_default_inverter():
    global _default_inverter
    if _default_inverter is None:
        _default_inverter = LaplaceInversion.Euler()
    return _default_inverter

def poisson(omega, fs, t, inverter=None):
    """
    Returns the log of the probability that exactly k-1 transitions have taken place
    in a window of time t, where k=len(fs). The transition rate for each
    Poisson process is omega/fs[i].

    Optionally specify a Laplace Inverter, the weights and nodes of which are
    used to generate a bespoke inversion formula (as the generic inversion
    performs poorly for large t). The default inverter is Euler().
    """

    if inverter is None:
        inverter = _make_default_inverter()

    k = len(fs)
    f0 = np.max(fs)
    omt = omega * t

    if k == 1: return - omt/f0

    return np.log(fs[-1]) + (k-1)*np.log(omt) - omt/f0 + np.log( (
        np.sum( inverter.weights / np.prod( np.outer(inverter.nodes, fs) + (1.0-fs/f0)*omt, axis=1) )
    ).real )

def poissongamma(alpha, beta, omega, fs, t, corrected=False, inverter=None):
    """
    Returns the log of the probability that exactly k-1 transitions have taken place
    in a window of time t, where k=len(fs). A transition comprises two parts, a Poisson
    initiation, at rate omega/fs[i], and a Gamma propagation with parameters alpha and beta.

    Optionally specify corrected=True to apply the interference correction.

    Optionally specify a Laplace Inverter, the weights and nodes of which are
    used to generate a bespoke inversion formula (as the generic inversion
    performs poorly for large t). The default inverter is Euler().
    """

    # TODO: overflow still seems to be possible under certain circumstances, but
    # these aren't triggered in the calculations we actually do, so we won't
    # pursue this just now.

    if inverter is None:
        inverter = _make_default_inverter()

    k = len(fs)
    f0 = np.max(fs)
    omt = omega*t
    bt  = beta *t

    if k == 1 and f0/omt + alpha/bt > 1.0:
        # Short-time regime: likelihood tends to unity, so we invert 1-likelihood to retain accuracy in the log
        # We also don't shift the contour here, as that seems to be more appropriate in the long-time regime
        return np.log1p( -np.sum( inverter.weights / inverter.nodes * (bt/(bt+inverter.nodes))**alpha * omt / (omt+f0*inverter.nodes)).real )

    # Here we are either in the long-time regime, or k>1. Either way, we are dominated by an exponential decay,
    # so shift the contour up to the singularity
    if beta < omega/f0:
        res = -bt
        snodes = inverter.nodes-bt
        logbnodesa = alpha * np.log(bt/inverter.nodes)
    else:
        res = -omt/f0 + np.log(f0)
        snodes = f0 * inverter.nodes - omt
        bt *= f0
        logbnodesa = alpha * np.log(bt/(bt+snodes))
        omt *= f0

    # Extract the overall scale of the result so that we never exponentiate this somewhere where it matters
    signs = np.sign(inverter.weights)
    logterms = np.log(np.abs(inverter.weights)) + (k-1)*logbnodesa + np.log1p( -np.exp(logbnodesa)*omt/(omt + snodes*fs[-1]) ) - np.log(snodes) - np.sum(np.log(omt + np.outer(snodes,fs[:-1])),axis=1)
    scale = np.max(logterms.real)
    res += (k-1)*np.log(omt) + scale + np.log( np.sum(signs * np.exp(logterms-scale)).real )

    return res - omega * alpha / beta * np.sum(1.0/fs[1:]) if corrected else res

# Minimal test case (compare computation against exact result)
if __name__ == '__main__':
    print(poisson(1.0, np.array([0.5]), 1.0), -2.0)
    print(poisson(1.0, np.array([0.5]*2), 1.0), np.log(2.0)-2.0)
