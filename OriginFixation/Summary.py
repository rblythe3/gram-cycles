"""
Classes to write summary statistics to a file and reload in
numpy-friendly form.
"""

import numpy as np

class Summariser(object):
    """
    An object that summarises a set of attributes to stdout, and
    echoes to file. Note, if filename is None, only the echoing takes place.
    """

    def __init__(self, filename, *args):
        "Initialise with the filename to write to and the set of fields to summarise as arguments"
        if filename is not None:
            self.fh = open(filename, 'w')
        else:
            self.fh = None
        header = '\t'.join(args)
        print(header)
        if self.fh:
            self.fh.write(header + '\n')
        self.fields = args

    def summarise(self, obj):
        "Summarise the set of fields provided as attributes of obj"
        writeout = '\t'.join( str(getattr(obj, field)) for field in self.fields )
        print(writeout)
        if self.fh:
            self.fh.write(writeout + '\n')

    def close(self):
        if self.fh:
            self.fh.close()


class Summary(object):
    """
    This object retrieves a summary from a named file
    converting each column to a numpy array that is
    stored as an attribute of this class.
    """

    def __init__(self, filename):
        "Load data from named file"

        with open(filename) as fh:
            colnames = next(fh).strip().split()
            table = np.loadtxt(fh)
        for name, data in zip(colnames, table.T):
            setattr(self, name, data)
