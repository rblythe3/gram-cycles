"""
Numerical Laplace inversion using the three methods from Abate and Whitt.
Here we provide a generic evaluation routine. For serious applications, it
seems to be better to read off the weights and nodes calculated by these objects
and use them yourself. For even more serious applications, we should probably
use a high-precision math library. This remains to be implemented.

Source: Abate and Whitt: A Unified Framework for Numerically Inverting Laplace Transforms
        INFORMS Journal on Computing 18(4), pp. 408 421 (2006)
        http://www.columbia.edu/~ww2040/AbateUnified2006.pdf
"""

import numpy as np
from scipy.special import binom, factorial

class Transform(object):
    """
    Class to represent the unified inverse Laplace transform expression.
    It relies on subclasses to define weights and nodes attributes.
    """

    def eval(self, F, t):
        "Evaluate the inverse Laplace transform of the ufunc F(s) at the timepoint t"
        return (np.sum( self.weights * F( self.nodes / t ) ) / t).real

    def f(self, F):
        "Return a Python function (NB: not a ufunc) that evaluates the inverse Laplace transform of the ufunc F(s)"
        return lambda t: self.eval(F,t)

class GaverStehfest(Transform):
    """
    This class implements the Gaver-Stehfest inverter.
    """

    def __init__(self, sigfigs=5):
        "Initialise the Gaver-Stehfest inverter for desired number of significant figures"
        M = int(np.ceil(1.1*sigfigs))
        self.weights = np.zeros(2*M)
        for n in range(2*M):
            k = n+1
            js = np.arange( (k+1)/2, min(k,M)+1 )
            self.weights[n] = (-1)**(M+k) * np.sum( js**(M+1) * binom(M, js) * binom(2*js,js) * binom(js,k-js) )
        self.weights *= np.log(2.0) / factorial(M)
        self.nodes = np.log(2.0) * (np.arange(2*M)+1)

    def __str__(self):
        return "Gaver-Stehfest"

class Euler(Transform):
    """
    This class implements the Euler inverter.
    """

    def __init__(self, sigfigs=5):
        "Initialise the Euler inverter for desired number of significant figures"
        M = int(np.ceil(1.8*sigfigs))
        xis = np.ones(2*M+1)
        xis[0] = 0.5
        xis[2*M:M:-1] = 1/2**M * np.cumsum(binom(M, np.arange(M)))
        self.weights = (-1)**np.arange(2*M+1) * xis * 10.0**(np.float_(M)/3.0)
        self.nodes = M * np.log(10.0) / 3.0 + np.pi*1j*np.arange(2*M+1)

    def __str__(self):
        return "Euler"


class Talbot(Transform):
    """
    This class implements the Talbot inverter.
    """

    def __init__(self, sigfigs=5):
        "Initialise the Talbot inverter for the desired number of significant figures"
        M = int(np.ceil(1.7*sigfigs))
        kpiM = np.arange(1,M)*np.pi/np.float_(M)
        cotkpiM = 1.0/np.tan(kpiM)
        self.nodes = np.zeros(M, dtype=np.complex_)
        self.nodes[0] = 0.4*M
        self.nodes[1:] = 0.4*np.pi*np.arange(1,M)*(cotkpiM+1j)
        self.weights = np.zeros(M, dtype=np.complex_)
        self.weights[0] = 0.5
        self.weights[1:] = 1.0 + 1j*(kpiM*(1.0+cotkpiM**2)-cotkpiM)
        self.weights *= 0.4 * np.exp(self.nodes)

    def __str__(self):
        return "Talbot"

# Perform a simple sanity check of the three algorithms

if __name__ == '__main__':

    # Test function: exponential-like

    def fwd(t): return 1.0-np.exp(-t)
    def bwd(s): return 1.0/s/(1.0+s)

    for inverter in [ GaverStehfest(), Euler(), Talbot() ]:

        print(inverter)

        print("\t", inverter.weights)
        print("\t", np.sum(inverter.weights))
        print("\t", inverter.nodes)

        # Columns two and three should be the same to the precision of the
        # inversion algorithm
        for t in np.linspace(0.1, 10):
            print("\t", t, fwd(t), inverter.eval(bwd, t))
        print()
