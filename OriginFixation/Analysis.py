"""
Likelihood analysis of a set of language changes according to a specific model.
"""

import numpy as np, scipy.optimize as spo, scipy.integrate as spi

from . import Distributions, DynamicInterpolation, Resources

class Results(object):
    """
    A results object holds the results of an analysis through a set of object attributes.
    These may depend on the analysis performed. Special attributes include
    loglikeilood, samplesize, parameters
    which if all defined permit computation of the aicc via the aicc attribute
    """

    def __init__(self, **kwargs):
        "Keyword arguments get turned into Results attributes"
        self.__dict__.update(kwargs)

    # Computed attributes
    @property
    def aicc(self):
        "Akaike information criterion, corrected for sample size"
        lnl, n, k = map(np.float_, (self.log_likelihood, self.sample_size, self.parameters))
        return 2.0*k - 2.0*lnl + 2.0*k*(k+1.0)/(n-k-1.0)

# Helper functions

def bracket(objective, x0, dx):
    """
    Bracket the minimum of objective by taking fixed-size steps of size dx
    starting from x0. This more cautious (but less intelligent) routine
    than that provided in scipy seems better suited for this application
    because we want to stay in the regime where the likeihood takes reasonable
    values.
    """
    w,x,y = x0-dx,x0,x0+dx
    a,b,c = map(objective, (w,x,y))
    while True:
        if a>b and b<c: return w,x,y
        if a<c:
            w,x = w-dx,w
            a,b = objective(w),a
        else:
            x,y = y,y+dx
            b,c = c,objective(y)

# Base class for analyses
class AnalysisBase(object):
    """
    An implementation should provide its own initaliser that sets up self.changes
    (the set of language changes) and provides the following methods:
        loglike_obschange(self, change, inno_rate, *params) - loglikelihood of an observed change
        loglike_nochange(self, change, inno_rate, *params) - loglikelihood of no change
        analyse(self, **kwargs) - optimise wrt inno_rate and return suitable Results object
    """

    def loglikelihood(self, inno_rate, *params):
        "Calculate the total log likelihood for the given innovation rate (and additional params, if any)"
        return sum( self.loglike_obschange(change, inno_rate, *params) for change in self.changes )

    def optimise(self, init_inno_rate, *params):
        "Maximise the likelihood with respect to innovation rate, given initial guess (and additional params, if any)"
        # Canned routines minimise wrt single variable, so need to flip sign.
        # Also optimise wrt log rate
        objective = lambda x: -self.loglikelihood(np.exp(x), *params)
        opt = spo.minimize_scalar( objective, bracket(objective, np.log(init_inno_rate), 0.1) )
        # Flip the sign back and exponentiate the answer
        opt.fun = -opt.fun
        opt.x = np.exp(opt.x)
        return opt

    def overdispersion(self, inno_rate, *params):
        "Calculate the mean overdispersion for the given innovation rate (and additional params, if any)"
        logover = np.float_('-inf')
        for change in self.changes:
            # Obtain log of the probability of no change
            logq = self.loglike_nochange(change, inno_rate, *params)
            if (logq >= 0.0 and change.states > 1) or (logq == np.float_('-inf') and changes.states == 1):
                return np.float_('inf') # Event that should never happen occured, so overdispersion is infinite...
            if logq >= 0.0 or logq == np.float_('-inf'):
                continue # Contribution to the sum is zero, so don't calculate it...
            logdover = logq - np.log1p(-np.exp(logq)) if logq<-1e-3 else -np.log(-logq)
            logover = np.logaddexp(logover, logdover if change.states > 1 else -logdover)
        return logover - np.log(len(self.changes)) # Converts sum to mean

    def finalise(self):
        "Finalisation: call to perform any cleanup/caching"
        # Default implementation is to do nothing
        pass


# Implementations

class Poisson(AnalysisBase):
    """
    The Poisson process is a purely macroscopic model with a constant rate of innovation.
    This analysis maximises the likelihood of the observed data with respect to the
    innovation rate.

    The reference population is irrelevant in the macroscopic analysis, but
    we take it as an argument for consistency
    """

    def __init__(self, languagechanges, stationary, reference):
        "Constant-rate Poisson process"
        self.changes = languagechanges
        self.stationary = stationary
        # An initial guess for the mean innovation rate: the total number of changes / total observation time
        self.omega_initial = np.sum( change.states-1 for change in self.changes ) / np.sum( change.years for change in self.changes )

    def loglike_obschange(self, change, omega):
        "Log likelihood of the observed sequence of changes with mean innovation rate omega"
        return Distributions.poisson(
            omega / len(self.stationary), # The parameter in Distributions.poisson isn't quite the mean innovation rate
            np.roll(self.stationary, -change.initial )[:change.states],
            change.years
        )

    def loglike_nochange(self, change, omega):
        "Log likelihood of the no change with mean innovation rate omega"
        return Distributions.poisson(
            omega / len(self.stationary), # The parameter in Distributions.poisson isn't quite the mean innovation rate
            [ self.stationary[change.initial] ],
            change.years
        )

    def analyse(self, **kwargs):
        "All keyword arguments are ignored; however they are permitted for compatability with other analyses"

        opt = self.optimise(self.omega_initial)

        return Results(
            log_likelihood = opt.fun,
            sample_size = len(self.changes),
            parameters = 1, # Only one parameter in the optimisation
            pop_innov_rate = opt.x, # Population-level innovation rate
            log_binary_overdispersion = self.overdispersion(opt.x) # Mean binary overdispersion, logged
        )


class InfiniteSelection(AnalysisBase):

    def __init__(self, languagechanges, stationary, reference):
        """
        Infinite-selection limit of a Wright-Fisher diffusion process
        (equivalent to a Poisson process with a linear dependence of the
        mutation rate on population size)
        """
        self.changes = languagechanges
        self.stationary = stationary
        self.reference = reference

        # An initial guess for the mean innovation rate: the total number of changes / total observation time / mean population size
        # (as we will be multiplying by population sizes; this is to keep numbers sane)
        self.omega_initial = (
            np.sum( change.states-1 for change in self.changes ) /
            np.sum( change.years for change in self.changes ) /
            np.mean( [change.size() for change in self.changes] )
        )

    def loglike_obschange(self, change, omega):
        "Log likelihood of the observed sequence of changes with mean innovation rate omega"
        return Distributions.poisson(
            change.size() * omega / len(self.stationary), # The parameter in Distributions.poisson isn't quite the mean innovation rate
            np.roll(self.stationary, -change.initial )[:change.states],
            change.years
        )

    def loglike_nochange(self, change, omega):
        "Log likelihood of the no change with mean innovation rate omega"
        return Distributions.poisson(
            change.size() * omega / len(self.stationary), # The parameter in Distributions.poisson isn't quite the mean innovation rate
            [ self.stationary[change.initial] ],
            change.years
        )

    def analyse(self, **kwargs):
        "All keyword arguments are ignored; however they are permitted for compatability with other analyses."

        opt = self.optimise(self.omega_initial)

        return Results(
            selection = kwargs['s'],
            interaction_rate = kwargs['R'],
            memory_size = kwargs['K'],
            network_exponent = kwargs['nu'],
            log_likelihood = opt.fun,
            sample_size = len(self.changes),
            parameters = 1, # Only one parameter in the optimisation
            pop_innov_rate = opt.x * self.reference.size(), # Population-level innovation rate
            ind_innov_rate = opt.x / kwargs['R'], # Individual-level innovation rate
            log_binary_overdispersion = self.overdispersion(opt.x) # Mean binary overdispersion, logged
        )

class WFBase(object):
    """
    Base class for the Wright-Fisher analysis that can be used more generally
    to relate to the Origin Fixation model.
    """

    # The version of the Wright-Fisher diffusion equation that is solved here takes
    # the form eta x(1-x) F_k' + x(1-x) F_k'' = - F_{k-1}.
    # where eta = 2 N_e s where N_e is the effective size of the population of stored
    # tokens = K * effective number of speakers.
    # We also eed to multiply all times by the characteristic timescale 2 N_e K / R
    # and multply the k^th moment by k! to get to the correctly dimensionalised
    # quantities used in the analysis.
    # This is all handled by the abo() method.

    def q(self,eta,x):
        "Fixation probability for rescaled selection strength eta"
        small = 1e-3
        if eta > small:
            if eta*x > small:
                return (1.0 - np.exp(-eta*x))/(1.0-np.exp(-eta))
            else:
                return eta*x * ( 1.0 - 0.5 * eta*x ) / (1.0 - np.exp(-eta))
        elif eta < -small:
            if eta*x < -small:
                return (np.exp(eta*(1.0-x))-np.exp(eta))/(1.0-np.exp(eta))
            else:
                return -eta*x * np.exp(eta) * ( 1.0 - 0.5 * eta*x ) / (1.0-np.exp(eta))
        else:
            return x * ( 1.0 + 0.5 * eta * (1.0-x) )

    # There are the integrals that must be performed numerically

    def _eval_tau1_0(self, eta):
        "Scaled first moment for p=0"
        return spi.quad( lambda y: (1.0-np.exp(-eta*(1.0-y))) * self.q(eta,y) / (y*(1-y)), 0, 1 )[0]

    def _eval_tau1(self, eta, p):
        "Scaled first joint moment for general p"
        if p <= 0.0 or p >= 1.0:
            return 0.0
        else:
            return (
                self.q(eta,p) * spi.quad( lambda y: (1.0-np.exp(-eta*(1.0-y))) * self.q(eta,y) / (y*(1-y)), p, 1 )[0] +
                self.q(eta,1.0-p) * spi.quad( lambda y: (np.exp(-eta*(p-y))-np.exp(-eta*p)) * self.q(eta,y) / (y*(1-y)), 0, p)[0]
            )

    def _eval_tau2_0(self, eta):
        "Scaled second moment"
        return spi.quad( lambda y: (1.0-np.exp(-eta*(1.0-y))) * self._eval_tau1(eta,y) / (y*(1-y)), 0, 1 )[0]

    # Useful constants
    _c1 = 1.0/72.0
    _c2 = np.pi**2.0/3.0 - 2.0
    _c3 = np.pi**2.0/36.0 - 17.0/54.0
    _c4 = np.pi**2.0/3.0

    # First conditioned moment
    def t1c(self, eta):
        "First conditioned moment"
        eta = np.abs(eta)
        if eta <= 1e-3: return 1.0 - self._c1 * eta**2
        elif eta >= 500.0: return 2.0*(np.log(eta)+np.euler_gamma - 1.0/eta)/eta
        else: return self._eval_tau1_0(eta) / eta

    # Second conditioned moment
    def t2c(self, eta):
        "Second conditioned moment"
        eta = np.abs(eta)
        if eta<= 1e-2: return self._c2 + self._c3 * eta*eta
        elif eta >= 500.0: return (4.0 * (np.log(eta)+np.euler_gamma)**2 + self._c4) / eta / eta
        else: return 2.0 * self._eval_tau2_0(eta) / eta / eta

    def __init__(self):
        "Set up the dynamic interpolation"
        self._eval_tau1_0 = DynamicInterpolation.DynamicInterpolation(
            self._eval_tau1_0, [1e-3,500.0], extendleft=False, extendright=False, cache=Resources.moment('first')
        )
        self._eval_tau2_0 = DynamicInterpolation.DynamicInterpolation(
            self._eval_tau2_0, [1e-2,500.0], extendleft=False, extendright=False, cache=Resources.moment('second')
        )

    def abo(self, Ns, mu, s, R, K, nu):
        """
        Obtain appropriate values for alpha, beta and omega in the Poisson-Gamma Distributions
        for number of speakers Ns, per-capita innovation probability nu, selection strength s, interaction rate R,
        memory size K and network exponent nu.
        """
        Nm = K * Ns # Number of memory slots in the population

        # Calculate network correction factor
        netf = 1.0
        if float(nu) != float('inf'):
            if nu <= 1.0: raise ValueError('Exponent nu must be larger than one')
            elif nu == 2.0: netf = 4.0 * (1.0-Ns**(-0.5))/np.log(Ns)
            else: netf = nu*(nu-2.0)/(nu-1.0)**2 * (1.0 - Ns**((1.0-nu)/nu))**2 / (1.0 - Ns**((2.0-nu)/nu))
        # Effective selection strength
        eta = 2.0*Nm*netf*s
        # Characteristic timescale of drift
        TD = 2.0*Nm*K*netf/R

        # Propagation probability
        Q = self.q(eta, 1.0/Nm)
        # First and second conditioned moments
        t1c = self.t1c(eta)
        t2c = self.t2c(eta)

        return (
            t1c*t1c/(t2c-t1c*t1c), # alpha
            t1c/TD/(t2c-t1c*t1c),  # beta
            Ns * mu * R * Q,       # omega
        )

    def cache(self):
        "Cache the interpolation state to speed things up next time"
        self._eval_tau1_0.cache()
        self._eval_tau2_0.cache()


class WrightFisher(AnalysisBase,WFBase):

    def __init__(self, languagechanges, stationary, reference):
        "Wright-Fisher diffusion process, as approximated by the Origin Fixation model"

        WFBase.__init__(self) # Fire up the WF engine

        self.changes = languagechanges
        self.stationary = stationary
        self.reference = reference

        # An initial guess for the mean innovation probability: the total number of changes / total observation time / mean population size
        # (as we will be multiplying by population sizes; this is to keep numbers sane)
        self.mu_initial = (
            np.sum( change.states-1 for change in self.changes ) /
            np.sum( change.years for change in self.changes ) /
            np.mean( [change.size() for change in self.changes] )
        )

    def loglike_obschange(self, change, mu, *params):
        """
        Calculate the log likelihood of the language changes with mean innovation probability per capita per interaction mu.
        Additional params are (s,R,K,nu) where
        s is selection strength, R is interaction rate R, K is memory size K and
        nu is network exponent
        """
        alpha, beta, omega = self.abo(change.size(), mu / len(self.stationary), *params)
        return Distributions.poissongamma(
            alpha, beta, omega,
            np.roll(self.stationary, -change.initial )[:change.states],
            change.years,
            corrected = True
        )

    def loglike_nochange(self, change, mu, *params):
        """
        Calculate the log likelihood of no language change with mean innovation probability per capita per interaction mu.
        Additional params are (s,R,K,nu) where
        s is selection strength, R is interaction rate R, K is memory size K and
        nu is network exponent
        """
        alpha, beta, omega = self.abo(change.size(), mu / len(self.stationary), *params)
        return Distributions.poissongamma(
            alpha, beta, omega, [ self.stationary[change.initial] ], change.years # No correction is required in this case
        )

    def analyse(self, **kwargs):
        """
        Perform the analysis within the Wright Fisher model with specified selection strength s,
        interaction rate R, memory size K and network exponent nu.
        """

        s, R, K, nu = ( kwargs['s'], kwargs['R'], kwargs['K'], kwargs['nu'] )

        opt = self.optimise(self.mu_initial, s, R, K, nu)

        # Get the alpha, beta, omega parameters at the optimum
        abo_opt = self.abo(self.reference.size(), opt.x, s, R, K, nu)

        return Results(
            selection = kwargs['s'],
            interaction_rate = kwargs['R'],
            memory_size = kwargs['K'],
            network_exponent = kwargs['nu'],
            log_likelihood = opt.fun,  # Flip the sign back
            sample_size = len(self.changes),
            parameters = 1, # Only one parameter in the optimisation
            pop_innov_rate = abo_opt[2], # omega here is the rate of successful innovations
            ind_innov_rate = opt.x,
            log_binary_overdispersion = self.overdispersion(opt.x, s, R, K, nu)
        )

    def finalise(self):
        "Finalisation: call to perform any cleanup/caching"
        self.cache() # The WFBase object will perform some caching
