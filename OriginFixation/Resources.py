"""
Locations of various resources relative to the base directory, plus
functions to store and retrieve objects at them.
"""

import pickle, os.path, glob

# Model data output

def wrightfisher(N, mu, s, mutate_twice=False, target=1, condition=False):
    "Output from the Wright-Fisher solver"
    return 'Output/WF/N{N}-mu{mu}-s{s}-mut{mut}-target{tar}{con}.dat'.format(
        N=N, mu=mu, s=s, mut=2 if mutate_twice else 1, tar=target, con='-C' if condition else ''
    )

# Language data input

def populations():
    "Input CSV file containing historical population sizes by region"
    return 'Input/populations.csv'

def wals(feature):
    "Input text file containing typological distributions in WALS"
    return 'Input/wals-{f}.dat'.format(f=feature)

def languages():
    "Input CSV file containing historical language change data"
    return 'Input/languages.csv'

# Cache files

def weightfit():
    "Weights of the various geographical regions"
    return 'Cache/weights.dat'

def populationcurve():
    "Population growth curve"
    return 'Cache/popcurve.dat'

def regionfit():
    "Fits to timeseries for geographical regions"
    return 'Cache/regionfit.dat'

def stationary(feature):
    "Stationary distributions of language features"
    return 'Cache/stationary-{f}.dat'.format(f=feature)

def compositions():
    "Compositions of speech communities"
    return 'Cache/compositions.dat'

def languagechanges(feature):
    "Combined language size and change data"
    return 'Cache/languagechanges-{f}.dat'.format(f=feature)

def moment(order):
    "Scaled conditional fixation time moments"
    return 'Cache/moment-{o}.dat'.format(o=order)

# Likeilhood analysis output

def summary(model, feature, tag):
    return 'Output/Summary/{m}-{f}-{t}.dat'.format(m=model, f=feature, t=tag)

def montecarlo(feature):
    return 'Output/MC/{f}.dat'.format(f=feature)

# Demonstration model output
# Returns a list of all relevant files if run is None
def demomodel(mu_bias, sig_bias, run=None):
    if run is None:
        return glob.glob('Output/Demo/r{r}-R{R}-run*.dat'.format(r=mu_bias,R=sig_bias))
    else:
        return 'Output/Demo/r{r}-R{R}-run{run}.dat'.format(r=mu_bias,R=sig_bias,run=run)

# Output for paper
def table(tag):
    return 'Output/Tables/{t}.tex'.format(t=tag)

def figure(filename):
    return 'Output/Figures/{f}'.format(f=filename)

# Functions for archiving and retrieval

def store(obj, location, depends=None):
    """
    Store the object passed as the first argument in the named location,
    optionally only if the file does not already exist and is not newer
    than any of the items listed in depends. (Supply an empty list to
    prevent overwriting any pre-existing file).

    Returns True if item is actually written to location, False if
    writing is prevented by the newer requirement. Will raise an error
    if file cannot be opened for writing.
    """
    if depends is not None and os.path.exists(location):
        mtime = os.path.getmtime(location)
        if all(os.path.getmtime(x) < mtime for x in depends):
            return False

    with open(location, 'wb') as fh:
        pickle.dump(obj, fh)
        return True

def retrieve(location):
    """
    Retrieve the object previously stored at the named location.
    Will raise an error if file cannot be opened for reading.
    """
    with open(location, 'rb') as fh:
        return pickle.load(fh)
