"""
Dynamic interpolation of a function of one variable.

When the function value is requested at a point, we look at neighbouring points
in the set of previous evaluations. If both points would have been correctly
approximated by a linear interpolation, we assume the new point will as well.
Otherwise, we perform a function evaluation and see how close the linear interpolation
would have been. After sufficiently many evaluations we should build up a good
approximation to the function.

By default, evaluations outside the initial range will extend the range over
which interpolation is possible; this can be prevented by setting extendleft
and/or extendright to False.
"""

import numpy as np, os.path, pickle

class DynamicInterpolation(object):

    def __init__(self, f, xs=None, tol=1e-4, extendleft=True, extendright=True, cache=None):
        """
        Create an interpolator for the function f, either afresh with a new mesh, or by
        reloading from a file specified by name.

        xs - initial mesh of points to evaluate f at; the min and max define the default interpolation region
        tol - maximum difference between the interpolated and actual value of a function to be considered as well approximated
        extendleft - allow interpolation below min(xs) if True
        extendright - allow interpolation above max(xs) if True
        cache - filename to load state from (if it exists), and to store state in on a call to cache()
        """

        self.f = f
        self.cachefile = cache
        if cache is not None and os.path.exists(cache):
            with open(cache, 'rb') as fh:
                self.extendleft = pickle.load(fh)
                self.extendright = pickle.load(fh)
                self.tol = pickle.load(fh)
                self.xs = pickle.load(fh)
                self.fs = pickle.load(fh)
                self.ok = pickle.load(fh)
        else:
            self.extendleft = extendleft
            self.extendright = extendright
            self.tol = tol
            self.xs = np.sort(xs)
            self.fs = np.vectorize(f)(self.xs)
            self.ok = np.zeros_like(self.xs,dtype=bool)

    def cache(self):
        """
        Store interpolation state in the file specified at creation time.
        """
        if self.cachefile is not None:
            with open(self.cachefile, 'wb') as fh:
                pickle.dump(self.extendleft, fh)
                pickle.dump(self.extendright, fh)
                pickle.dump(self.tol, fh)
                pickle.dump(self.xs, fh)
                pickle.dump(self.fs, fh)
                pickle.dump(self.ok, fh)

    # Helper functions

    # Test if two points are sufficiently close to each other
    def _is_close(self, a, b):
        return np.abs(a-b) < self.tol or (np.abs(a)>0.0 and np.abs(b)>0.0 and np.abs(a-b) < np.min(np.abs([a,b]))*self.tol)

    # Test whether the point at n is now ok
    def _make_ok(self, n):
        # Form linear interpolation between points either side, and if within tol of cached f, make ok
        d = (self.xs[n] - self.xs[n-1])/(self.xs[n+1] - self.xs[n-1])
        fe = (1.0-d)*self.fs[n-1] + d*self.fs[n+1]
        if self._is_close(self.fs[n],fe):
            self.ok[n] = True

    def __call__(self, x):
        """
        Obtain an estimate of the function f passed at creation at the point x.
        This will be achieved either by interpolating from nearby points, or
        by an evaluation of the underlying function.
        """

        fx = None
        if x < self.xs[0]:
            fx = self.f(x)
            # Insert (x,fx) at head of list if we want the interpolator to do that
            if self.extendleft:
                self.xs = np.insert(self.xs,0,x)
                self.fs = np.insert(self.fs,0,fx)
                self.ok = np.insert(self.ok,0,False)
                if not self.ok[1]: self._make_ok(1)
            return fx
        elif x > self.xs[-1]:
            fx = self.f(x)
            # Insert (x,fx) at tail of list if we want the interpolator to do that
            if self.extendright:
                self.xs = np.append(self.xs,x)
                self.fs = np.append(self.fs,fx)
                self.ok = np.append(self.ok,False)
                if not self.ok[-2]: self._make_ok(len(self.ok)-2)
            return fx
        else:
            # Find position in list where x would go if inserted
            n = np.searchsorted(self.xs, x)
            if x == self.xs[n]: return self.fs[n]
            # Build linear interpolation
            d = (x-self.xs[n-1])/(self.xs[n]-self.xs[n-1])
            fe = (1.0-d)*self.fs[n-1] + d*self.fs[n]
            if self.ok[n-1] and self.ok[n]:
                return fe
            # The boundary values can never become safe, but if they are sufficiently close we can still interpolate
            elif n == 1 and self._is_close(self.fs[0],self.fs[1]):
                return fe
            elif n == len(self.xs) and self._is_close(self.fs[n-1],self.fs[n]):
                return fe
            else:
                fx = self.f(x)
                self.xs = np.insert(self.xs,n,x)
                self.fs = np.insert(self.fs,n,fx)
                if self._is_close(fx,fe):
                    self.ok = np.insert(self.ok,n,True)
                    if n>1 and not self.ok[n-1]: self._make_ok(n-1)
                    if n<len(self.ok)-2 and not self.ok[n+1]: self._make_ok(n+1)
                else:
                    self.ok = np.insert(self.ok,n,False)
                return fx
