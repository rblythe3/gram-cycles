#
# A class to encapsulate a language change for a specific grammatical feature
#
"""
Class to encapsulate historical observations of change in a language along
with other relevant properties, particularly its size, which is computed
with the help of a PopulationCurve object.
"""

import numpy as np, scipy.integrate as spi

class LanguageChange(object):

    def __init__(self, language, weight, periods, initial, states, root, references):
        """
        language: a name to identify the language by
        weight: the relative size of the speech community
        periods: periods over which the language changes were observed (array of (start, stop) pairs)
        initial: the stage of the grammaticalisation cycle at the start of the first observation window
        states: the number of states observed (1=remains in the initial state, 2=changes once, etc)
        root: True if this language is at the root of a tree of descendent languages; False otherwise
        references: References in support of these data; list of pairs (bibtexkey, pages)
        """

        # You may directly access these attributes
        self.language = language
        self.weight = weight
        self.periods = periods
        self.initial = initial
        self.states = states
        self.root = root
        self.years = np.sum( (e-s) for s,e in periods )
        self.references = references

        # You may request a size to be cached to save recomputing it every time
        self.cached_size = None

    # Give the size of the population
    #  t is a specific time point; or
    #  moment is mth moment <T^m> over the relevant interval(s)
    # If you call with cache=True then if you call again with no arguments you'll get the previous value back
    #
    def size(self,popcurve=None,t=None,moment=None,cache=False):
        """
        If called with no arguments, return a previously cached population size.

        Otherwise, the arguments are:
        popcurve: a PopulationCurve object used to construct population size estimates.
        t: time point to obtain size estimate; or
        moment: obtain this moment of the population size over the entire observation period
        cache: set to True if you want subsequent calls with no arguments to return the same size.
        """

        if popcurve is None:
            if self.cached_size is not None:
                return self.cached_size
            else:
                raise ValueError('No cached value to return')

        res = None

        if moment is not None:
            res = ( np.sum( spi.quad( lambda t: popcurve(self.weight, t)**moment, s, e)[0] for s,e in self.periods ) / self.years )
        elif t is not None:
            res = popcurve(self.weight, t)

        if cache:
            self.cached_size = res

        if res is None:
            raise ValueError('Must provide a time point or a moment')

        return res

    def __repr__(self):
        return 'LanguageChange({l},{w},{p},{y},{i},{s},{r})'.format(l=self.language,w=self.weight,p=self.periods,y=self.years,i=self.initial,s=self.states,r=self.root)
