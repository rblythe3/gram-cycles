"""
A DataTable comprises a sequence of rows, each of which comprises
a set of named columns. The rows may optionally be labelled by a key
which is derived from one of the columns via a user-supplied transformation
function. Column names are always downcased.

The table is initialised by reading from a CSV file, the first row of which
is used to name the columns.

Example: T = DataTable(fromCSV='file.csv', key='name', ktransform=lambda s: s[0])
with file.csv containing

Name,Age
Dave,23
Emma,35
Fred,19
Gill,22

would then yield

len(T) -> 4
T['F'] -> {'name':'Fred', 'age':19}
T['Ellie'] -> {'name':'Emma', 'age':35}
'Bert' in T -> False

Note that keys are also transformed at lookup and the order of the rows
is preserved only if no key is provided (in which case integers are used
as keys instead).
"""

import csv

class DataTable(object):
    def __init__(self, fromCSV, key=None, keytransform=None):
        """
        Load the DataTable from a CSV file named fromCSV.
        Optionally allow indexing by the column named in key, further
        optionally transformed with the function keytransform.

        A RuntimeError is raised if more than one row would be referenced
        by the same key.
        """

        self.rows = None
        self.kxform = None

        with open(fromCSV, 'r') as f:
            reader = csv.reader(f) # We don't use the DictReader as we want to transform the keys
            keys = [ s.lower() for s in next(reader) ]
            if key is None:
                self.rows = []
            else:
                self.rows = {}
                self.kxform = keytransform
                if key not in keys:
                    key = keys[key] # This will give an error if key is not numeric or out of range
            for row in reader:
                row = dict(zip(keys,row))
                if key is None:
                    self.rows += [row]
                else:
                    ckey = row[key] if self.kxform is None else self.kxform(row[key])
                    if ckey in self.rows:
                        raise RuntimeError("Key clash '{c}', derived from '{k}' in {f}".format(c=ckey,k=row[key],f=fromCSV))
                    self.rows[ckey] = row

    # Container magic methods

    def __len__(self):
        "Return the number of rows in the table"
        return 0 if self.rows is None else len(self.rows)

    def __getitem__(self,key):
        "Obtain the row indexed with the specified key (after applying the key transformation)"
        if self.kxform is not None:
            key = self.kxform(key)
        return None if self.rows is None else self.rows[key]

    def __iter__(self):
        "Iterate over the rows"
        return iter(self.rows.values()) if type(self.rows) is dict else iter(self.rows)

    def __contains__(self,key):
        "Determine whether a row indexed by the specified key exists"
        if self.kxform is not None:
            key = self.kxform(key)
        return False if self.rows is None else key in self.rows
