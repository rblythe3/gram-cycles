#!/usr/bin/python3
"""
Visualise the normalisation and fit to historical population size data.
This is Figure 1 in the appendix.
"""

import argparse, matplotlib.pyplot as plt, numpy as np
from OriginFixation import Resources

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--fig', nargs='?', const='FigS1_1.pdf', help='Output figure to file [off]')
args = parser.parse_args()

# Retrieve the cached data for plotting
visfit = Resources.retrieve(Resources.regionfit())
popcurve = Resources.retrieve(Resources.populationcurve())

label_once = 'Observations' # Ensure that we end up with a sensible legend

for location in visfit:
    times, observed, norm, fit = visfit[location]
    if location == '*':
        plt.semilogy(times, norm, 's', mec='black', mfc='none', mew=2, ms=8, zorder=2, label='Mean')
    else:
        plt.semilogy(times, norm, 'o', color='0.75', mec='none', ms=4, zorder=0, label=label_once)
        label_once=None

alltimes = np.linspace( min(visfit['*'][0]), max(visfit['*'][0]), 100)
plt.semilogy(alltimes, [ popcurve(1.0, t) for t in alltimes ], 'b-', lw=2, zorder=1, label='Polynomial fit')

plt.xlabel('Year, relative to 1CE')
plt.ylabel('Normalised population size')

plt.legend()

if args.fig:
    plt.savefig(Resources.figure(args.fig))
    print('Output written in {where}'.format(where=Resources.figure(args.fig)))
else:
    plt.show()
