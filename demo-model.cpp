/**
  * Demonstration that the Wright-Fisher model is a universal attractor.
  */

#include <iostream>
#include <vector>
#include <queue>
#include <functional>
#include <list>
#include <unordered_map>
#include <map>
#include <random>
#include <cmath>
#include <valarray>

#include "CLI11.hpp"

#include <cassert>


// The Scheduler manages the waiting-time algorithm. Events are modelled as
// C++ function objects; these are called at the scheduled (simulation) time.
// The advance method advances to the next scheduled event and executes it.
class Scheduler {

public:

  using Event = std::function<void()>;
  using EventId = unsigned;

private:
  double _time = 0.0;

  using TimedEvent = std::pair<double, Event>;

  static bool cmp(const TimedEvent& a, const TimedEvent& b) {
      return a.first > b.first;
  }

  std::priority_queue<TimedEvent, std::vector<TimedEvent>, std::function<bool(const TimedEvent&, const TimedEvent&)>> queue;

public:

  Scheduler() : queue(cmp) { }

  // Schedule the event e to fire after the specified delay
  void schedule(double delay, Event e) {
    queue.push(std::make_pair(_time+delay, e));
  }

  // Advance the clock to the next event and execute it
  // Returns false if there was no event to advance to
  bool advance() {
    if(!queue.empty()) {
      auto te = queue.top();
      queue.pop();
      _time = te.first;
      te.second();
      return true;
    }
    return false;
  }

  double time() const {
    return _time;
  }

};

// Represents a speaker
struct Speaker {
  double x;            // Frequency of using variant 0
  double born;         // Time at which this agent entered the population
  double age_at_death; // Age at which this agent will die
  double rate0;        // Initial interaction rate
  double decay;        // Decay rate of interaction rate
  double rate1;        // Asymptotic interaction rate
  double replace;      // Fraction of frequency which is replaced
  double bias;         // Bias applied to innovation

  // Mass of interactions between t0 and t1 (t=0 is time of birth)
  double mass(double t0, double t1) const {
    return rate1*(t1-t0) + (rate0-rate1)/decay * (std::exp(-decay*t0) - std::exp(-decay*t1));
  }

};

struct Parameters {
  unsigned regulate       = 1000; // Population size at which birth rate regulation kicks in
  double mu_offspring     =  2.0; // Mean offspring number (in absence of competition)
  double mu_birth         = 30.0; // Mean offspring age
  double sig_birth        =  8.0; // Standard deviation in offspring age
  double mu_expand        = 18.0; // Mean age of network expansion
  double sig_expand       =  4.0; // Standard deviation in network expansion
  double mu_death         = 60.0; // Mean age of death
  double sig_death        = 12.0; // Standard deviation in age of death
  double mu_inherit       =  3.0; // Mean number of interlocutors after inheritance
  double homophily        =  0.2; // How much network expansion is biased by age
  double retain           =  1.0; // Weight given to inherited interlocutors
  double mu_interlocutors = 10.0; // Mean number of interlocutors after expansion
  double mu_rate0         =  1.0; // Mean value of the initial interaction rate
  double sig_rate0        =  0.1; // Standard deviation in the initial interaction rate
  double mu_ratio         = 10.0; // Mean ratio between the initial and asymptotic interaction rate
  double sig_ratio        = 10.0; // Standard deviation in the initial and asymptotic interaction rate
  double mu_decaytime     = 20.0; // Mean decay time for interaction rate
  double sig_decaytime    = 10.0; // Standard deviation in decay time for interaction rate
  double mu_replace       =  0.15; // Mean of the replacement fraction distribution
  double sig_replace      =  0.1;  // Standard deviation of the replacement fraction distribution
  double p_listen         =  0.5;  // Probability any one interlocutor learned from in an interaction
  double mu_bias          =  0.0;  // Mean innovation bias
  double sig_bias         =  0.0;  // Standard deviation in innovation bias
};

// Simple contact matrix helper class
class ContactMatrix {
  double max_age; // Maximum age of a speaker who gets binned
  unsigned _bins;  // Number of bins
  std::valarray<unsigned> nhearers; // Number of hearers in each age class

  // contacts[bins*h+s] = total number of speakers in age class s that hearer in age bin h learn from
  std::valarray<unsigned> contacts;

public:
  ContactMatrix(double max_age, unsigned bins) : max_age(max_age), _bins(bins), nhearers(bins), contacts(bins*bins) { }

  // Record a hearing agent
  void add_hearer(double hearer_age) {
    unsigned h = unsigned(hearer_age/max_age*_bins);
    if(h<_bins) ++nhearers[h];
  }

  // Record a contact
  void add_contact(double hearer_age, double speaker_age) {
    unsigned h = unsigned(hearer_age/max_age*_bins);
    unsigned s = unsigned(speaker_age/max_age*_bins);
    if(h>=_bins || s>=_bins) return; // ignore contacts beyond max_age
    ++contacts[_bins*h+s];
  }

  // Number of bins
  unsigned bins() { return _bins; }

  // Return start and end age of the kth bin
  std::pair<double, double> bin(unsigned k) {
    return {k*max_age/_bins, (k+1)*max_age/_bins};
  }

  // Return number of hearers in an age category
  double hearers(unsigned h) {
    return nhearers[h];
  }

  // Return mean number of contacts per hearer in age bin h to speakers in age bin s
  // We get zero if there are no hearers
  double operator () (unsigned h, unsigned s) {
    return nhearers[h]>0 ? double(contacts[_bins*h+s])/double(nhearers[h]) : 0.0;
  }

  // Combine the counts from another ContactMatrix
  ContactMatrix& operator |= (const ContactMatrix& other) {
    assert(other.max_age == max_age);
    assert(other._bins == _bins);
    nhearers += other.nhearers;
    contacts += other.contacts;
    return *this;
  }
};

// Helper class to give us a beta distribution
template<typename T>
class beta_distribution {
  std::gamma_distribution<T> xdist, ydist;
public:
  beta_distribution(double alpha, double beta) : xdist(alpha, 1.0), ydist(beta, 1.0) { }

  template<typename Engine>
  T operator () (Engine& rng) {
    T x = xdist(rng);
    T y = ydist(rng);
    return x/(x+y);
  }
};

// Represents a speech community
template<typename Engine>
class Community {
  using sid = unsigned; // Speaker id type

  sid next_id = 0; // id of the next speaker to be inserted

  Scheduler S;
  Engine rng;

  Parameters P; // System parameters

  std::unordered_map<sid, Speaker> speakers;
  mutable std::unordered_map<sid, std::list<sid>> interlocutors;

  // Insert a new speaker into the community, returns unique id
  sid insert(const Speaker& speaker) {
    sid s = ++next_id;
    speakers[s] = speaker; // This takes a copy
    interlocutors[s] = std::list<sid>(); // This should move the temporary list
    return s;
  }

  // Remove a speaker from the community
  void remove(sid speaker) {
    speakers.erase(speaker);
    interlocutors.erase(speaker);
  }

  // Add an interlocutor relationship
  void add_interloctor(sid hearer, sid speaker) {
    interlocutors[hearer].emplace_back(speaker);
  }

  // Remove an interlocutor relationship
  void remove_interlocutor(sid hearer, sid speaker) {
    std::remove(interlocutors[hearer].begin(), interlocutors[hearer].end(), speaker);
  }

  // Remove interlocutors for speakers who no longer exist
  // Returns the number of interlocutors remaining
  unsigned clean_interlocutors(sid hearer) const {
    interlocutors[hearer].remove_if(
      [this](sid s){ return speakers.count(s) == 0; }
    );
    return interlocutors[hearer].size();
  }

  // Random number distributions
  std::gamma_distribution<double> birthdist, expanddist, deathdist, rate0dist, ratiodist, decaydist;
  beta_distribution<double> replacedist;
  std::normal_distribution<double> biasdist;
  std::geometric_distribution<unsigned> offspringdist;

  // Whether agents interact
  bool _interacts = false;

  // Create the overall lifecycle for an agent, optionally inheriting from
  // a specified parent
  void create_lifecycle(bool inherit=false, sid parent=0) {

    if(size() > P.regulate) {
      // Apply birth-rate limiter
      if(!std::bernoulli_distribution(double(P.regulate)/double(size())/P.mu_offspring)(rng)) return;
    }

    auto now = S.time();
    auto age_at_death = deathdist(rng);
    auto rate0 = rate0dist(rng);
    auto s = insert({ // Speaker object;
      inherit ? speakers[parent].x : 0.5, // Initial frequency
      now,                        // time of birth
      age_at_death,               // age at death
      rate0,                      // initial interaction rate
      1.0/decaydist(rng),         // decay of interaction rate
      rate0/(1.0+ratiodist(rng)), // asymptotic interaction rate
      replacedist(rng),           // replacement fraction
      biasdist(rng)               // innovation bias
    });

    // Event callback for death
    S.schedule(age_at_death, [s,this](){
      if(size() == 1) create_lifecycle(true, s); // Last surviving member gets cloned to avoid extinction
      remove(s);
    });

    // Schedule the creation of offspring: we keep trying until all the desired offspring are actually created
    auto offspring = offspringdist(rng);
    while(offspring>0) {
      auto age_at_birth = birthdist(rng); // parent's age at child's birth!
      if(age_at_birth<age_at_death) {
        S.schedule(age_at_birth, [s,this](){ create_lifecycle(true, s); });
        --offspring;
      }
    }

    if(inherit) {
      // Child will always have parent as interlocutor
      add_interloctor(s, parent);
      if(P.mu_inherit >= interlocutors[parent].size()) {
        // Add all interlocutors in this case (could make this more random I guess)
        for(const auto& i: interlocutors[parent]) add_interloctor(s, i);
      } else {
        // Add each interlocutor with probability mu_inherit / # parent interlocutors, capped at 1
        std::bernoulli_distribution inheritdist(P.mu_inherit/interlocutors[parent].size());
        for(const auto& i: interlocutors[parent]) {
          if(inheritdist(rng)) add_interloctor(s, i);
        }
      }
    }

    // Schedule expansion of social network
    auto age_at_expand = expanddist(rng);
    if(age_at_expand < age_at_death) {
      S.schedule(age_at_expand, [s,this](){ expand(s); });
    }

    // If this is an interacting agent, make interactive
    if(_interacts) create_interaction(s);
  }

  void expand(sid s) {
    // Expand a speaker's social network: give every member of the community a score
    // based on difference in age from s, and also on whether they are already connected
    // Normalise the scores to mu_interlocutors. Bernoulli sample the normalised scores.
    // We never said this was going to be fast.
    std::unordered_map<sid, double> weights;
    double norm = 0.0;
    // Prime with weight of original interlocutors (if they are still alive)
    for(const auto& o: interlocutors[s]) {
      if(speakers.count(o)) norm += (weights[o] = P.retain);
    }
    // Now consider other members of the community weighted exponentially by age difference
    double my_dob = speakers[s].born;
    for(const auto& ss: speakers) {
      if(ss.first == s || weights.count(ss.first)) continue; // Skip self and existing interlocutors
      norm += (weights[ss.first] = std::exp(-P.homophily*std::abs(ss.second.born-my_dob)));
    }
    // Remove all existing interlocutors
    interlocutors[s].clear();
    if(norm > 0.0) {
      // Further renormalise, so that sum of weights is mu_interlocutors
      norm = P.mu_interlocutors / norm;
      // Now rebuild according to probabilities
      for(const auto& sw: weights) {
        double p = sw.second*norm;
        if(p>=1.0 || std::bernoulli_distribution(p)(rng)) add_interloctor(s, sw.first);
      }
    }
  }

  std::exponential_distribution<double> expdist;

  // Create an interaction event for a given hearer
  void create_interaction(sid hearer) {

    // Determine the age of the speaker when they next interact
    const auto& hprops = speakers[hearer]; // Properties of this hearer

    // Current age of speakers
    double age = S.time() - hprops.born;

    // Generate a standard exponentially-distributed number
    double tau = expdist(rng);

    // Convert to age at which next interaction will happen
    double t;

    if(hprops.decay>0.0) {
      // Bracket on when the mass hits tau; fudge factor on upper bound just in case we are in asymptotic regime
      // and we get something that numerically isn't quite a bracket...
      double tlo = age, thi = age + 1.2*tau / hprops.rate1;
      assert(hprops.mass(age, tlo) <= tau);
      assert(hprops.mass(age, thi) >= tau);

      while(thi-tlo > 1e-9) {
        t = 0.5*(tlo+thi); // Simple bisection search
        double obj = hprops.mass(age, t) - tau;
        if(obj > 1e-6) thi = t;
        else if(obj < -1e-6) tlo = t;
        else break;
      }
    } else {
      // Treat as rate0 at all times
      t = age + tau / hprops.rate0;
    }

    // Schedule an interaction after delay t-age
    if(t < hprops.age_at_death) {
      S.schedule(t-age, [hearer,this]() { interact(hearer); });
    }

  }

  std::bernoulli_distribution listendist;

  void interact(sid hearer) {
    if(interlocutors[hearer].size() == 0) return; // Nothing to do
    auto& hprops = speakers[hearer]; // Properties of this hearer

    double x = 0.0; // Frequency of the innovation in the speaker's immediate environment
    bool accumulating = false; // Failsafe to ensure at least one speaker gets listened to
    unsigned n = 0; // Number of speakers considered so far
    unsigned m = 0; // Number of speakers actually listened to so far

    for(auto sit = interlocutors[hearer].begin(); sit != interlocutors[hearer].end();) {
      if(speakers.count(*sit) == 0) {
        sit = interlocutors[hearer].erase(sit); // May as well clean dead speakers here
        continue;
      }
      if(listendist(rng)) {
        // This speaker listened to by the stochastic inclusion rule;
        // start or continue accumulating
        if(accumulating) {
          x += speakers[*sit].x;
          ++m;
        }
        else {
          x = speakers[*sit].x;
          m = 1;
          accumulating = true; // Keep adding!
        }
      } else if(!accumulating) {
        // In the event that the stochastic rule doesn't select a speaker, we will
        // fall back on this speaker's frequency with probability 1/(n+1). This is equivalent
        // to a uniform choice.
        if(std::uniform_int_distribution<decltype(n)>(1,n+1)(rng) == 1) {
          x = speakers[*sit].x;
          m = 1;
        }
      }
      ++n;
      ++sit;
    }
    if(n==0) return; // Turns out interlocutors were all dead...
    x/=m;

    // Now decide whether the target is the innovation (1.0) or the convention (0.0)
    double target = std::bernoulli_distribution((1.0+hprops.bias)*x/(1.0+hprops.bias*x))(rng) ? 1.0 : 0.0;

    // Replace the corresponding fraction of the store
    hprops.x += hprops.replace * (target - hprops.x);
    // Schedule another interaction
    create_interaction(hearer);
  }

public:

  Community(const Parameters& params) :
    rng((std::random_device())()),
    P(params),
    birthdist(P.mu_birth/P.sig_birth*P.mu_birth/P.sig_birth, P.sig_birth*P.sig_birth/P.mu_birth),
    expanddist(P.mu_expand/P.sig_expand*P.mu_expand/P.sig_expand, P.sig_expand*P.sig_expand/P.mu_expand),
    deathdist(P.mu_death/P.sig_death*P.mu_death/P.sig_death, P.sig_death*P.sig_death/P.mu_death),
    rate0dist(P.mu_rate0/P.sig_rate0*P.mu_rate0/P.sig_rate0, P.sig_rate0*P.sig_rate0/P.mu_rate0),
    ratiodist(P.mu_ratio/P.sig_ratio*P.mu_ratio/P.sig_ratio, P.sig_ratio*P.sig_ratio/P.mu_ratio),
    decaydist(P.mu_decaytime/P.sig_decaytime*P.mu_decaytime/P.sig_decaytime, P.sig_decaytime*P.sig_decaytime/P.mu_decaytime),
    replacedist(
      (P.mu_replace*(1.0-P.mu_replace)/P.sig_replace/P.sig_replace-1.0)*P.mu_replace,
      (P.mu_replace*(1.0-P.mu_replace)/P.sig_replace/P.sig_replace-1.0)*(1.0-P.mu_replace)
    ),
    biasdist(P.mu_bias, P.sig_bias),
    offspringdist(1.0/(1.0+P.mu_offspring)),
    listendist(P.p_listen)
  {
    // Seed with a single agent
    create_lifecycle();
  }

  // Execute scheduled events until the stated time
  double run_until(double until) {
    while(S.time() < until && S.advance());
    return S.time();
  }

  // Whether agents interact
  bool interacts() const {
    return _interacts;
  }

  void interacts(bool i) {
    _interacts = i;
  }

  // Distribute variants randomly across population
  // p = prob each agent is ascribed 0 or 1
  // return actual frequency across the population
  double set_inno(double p) {
    std::bernoulli_distribution inno(p);
    double x = 0.0;
    for(auto& sp: speakers) {
      x += (sp.second.x = inno(rng) ? 1.0 : 0.0);
    }
    return x / size();
  }

  // Summary statistics

  unsigned size() const {
    return speakers.size();
  }

  double mean_degree() const {
    if(size()==0) return 0.0;
    // Go through and clean all interlocutors and count them
    unsigned sum_degree = 0;
    for(const auto& s: speakers) sum_degree += clean_interlocutors(s.first);
    return static_cast<double>(sum_degree)/static_cast<double>(size());
  }

  std::map<unsigned, unsigned> degree_distribution() {
    std::map<unsigned, unsigned> ddist;
    for(const auto& s: speakers) ++ddist[clean_interlocutors(s.first)];
    return ddist;
  }

  // Mean frequency of the innovation
  double mean_inno() {
    return std::accumulate(
      speakers.begin(), speakers.end(), 0.0, [](const double x, const typename decltype(speakers)::value_type& s) {return x+s.second.x;}
    ) / size();
  }

  // Obtain a matrix of contacts with the specified maximum age and number of bins
  ContactMatrix contacts(double max_age, unsigned bins) {
    ContactMatrix cmat(max_age, bins);
    for(const auto& h: speakers) {
      double hage = S.time() - h.second.born;
      cmat.add_hearer(hage);
      for(const auto& s: interlocutors[h.first]) {
        if(speakers.count(s)>0) cmat.add_contact(hage, S.time() - speakers[s].born);
      }
    }
    return cmat;
  }

};

int main(int argc, char* argv[]) {

  Parameters P;

  // We will expose the model parameters to the command line in due course; but in the meantime we'll
  // just allow access to basic run parameters

  CLI::App app{"Demonstration speech community model"};

  double burnin = 1000;    // Burnin time
  unsigned bins = 100;     // Number of frequency/age bins
  unsigned measure = 1000; // Number of measurements to take
  double spacing = 10.0;   // Time between measurements

  bool contact = false;    // Measure contacts (otherwise measure jump moments)

  double inno   = 0.5;     // Initial fraction of innovators
  double boundary = 1e-5;  // Distance from 0 or 1 to consider the boundary reached

  double max_age = 100.0;  // Maximum age for contact matrix

  // Expose (some of) the model parameters
  app.add_option("-r,--meanrbias", P.mu_bias, "Mean replicator bias");
  app.add_option("-R,--stdrbias", P.sig_bias, "Standard error on replicator bias");

  app.add_option("-b,--burnin",   burnin,  "Time to grow speech community");
  app.add_option("-n,--bins",     bins,    "Number of frequency/age bins");
  app.add_option("-m,--measure",  measure, "Number of jump moment measurements");
  app.add_option("-s,--spacing",  spacing, "Spacing between jump moment measurements");

  app.add_flag("-C,--contact,-J{false},--jumpmoments{false}", contact, "Measure contact matrices instead of jump moments");

  app.add_option("-i,--inno",     inno,    "Fraction of innovators assigned after burnin [jump moments only]");
  app.add_option("-B,--boundary", boundary, "Distance from boundary at which simulation is terminated [jump moments only]");

  app.add_option("-a,--maxage",   max_age, "Maximum age for contact matrix [contact matrix only]");

  CLI11_PARSE(app, argc, argv);

  // Set up the speech community
  Community<std::mt19937> C(P);

  // Initial burnin
  C.run_until(burnin);

  if(!contact) {
    // Initialisation of innovators for jump moment measurements
    C.set_inno(inno);
    C.interacts(true);

    // Set up measurement arrays (these are all value (=zero) initialised)
    std::valarray<unsigned> count(bins);
    std::valarray<double>   m1(bins);
    std::valarray<double>   m2(bins);

    double x0 = C.mean_inno();

    for(unsigned k=1; k<=measure; ++k) {
      if(x0<boundary || x0>1.0-boundary) break;
      C.run_until(burnin+k*spacing);
      double x = C.mean_inno();
      unsigned bin = static_cast<unsigned>(x0*bins);
      assert(bin<bins);
      ++count[bin];
      double dx = x - x0;
      m1[bin] += dx;
      m2[bin] += dx*dx;
      x0 = x;
    }
    // We output raw data, so that we can more easily amalgamate output from different
    // runs or filter on empty bins.
    for(unsigned b=0; b<bins; ++b) {
      std::cout << static_cast<double>(b)/static_cast<double>(bins) << "\t";
      std::cout << count[b] << "\t" << m1[b] << "\t" << m2[b] << std::endl;
    }


  } else {
    ContactMatrix cm(max_age, bins);

    for(unsigned k=1; k<=measure; ++k) {
      C.run_until(burnin+k*spacing);
      cm |= C.contacts(max_age, bins);
    }

    // Output the averaged contact matrix.
    // Each row is a bin; columns are binstart, binend, number of hearers, mean number of speakers by bin
    for(unsigned h=0; h<bins; ++h) {
      const auto binpos = cm.bin(h);
      std::cout << binpos.first << "\t" << binpos.second << "\t" << cm.hearers(h);
      for(unsigned s=0; s<bins; ++s) {
        std::cout << "\t" << cm(h,s);
      }
      std::cout << std::endl;
    }
  }

  return 0;
}
