#!/usr/bin/python3
"""
Visualise the residuals to the fit of the population growth model.
This is figure 2 in the appendix.
"""

import argparse, matplotlib.pyplot as plt, numpy as np
from OriginFixation import Resources

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--fig', nargs='?', const='FigS1_2.pdf', help='Output figure to file [off]')
args = parser.parse_args()


# Retrieve the cached data for plotting
visfit = Resources.retrieve(Resources.regionfit())

# Build up (log) residuals
resid = np.empty(0)
for location in visfit:
    times, observed, norm, fit = map(np.array, visfit[location])
    if location == '*':
        continue
    resid = np.append(resid, np.log(observed) - np.log(fit))

bins = 30
range = (-2.0,2.0)

plt.hist(resid,bins=bins,range=range,density=True, fc='0.7')

# Get the mean and sample variance
mean = np.mean(resid)
var = np.var(resid,ddof=1)
print('Mean: {m}. Stdev: {d}'.format(m=mean,d=np.sqrt(var)))

# Plot the normal distribution with same mean and variance
gaussian = lambda x: 1.0/np.sqrt(2.0*np.pi*var)*np.exp(-(x-mean)**2/2.0/var)
xs = np.linspace(*range, num=100)
plt.plot( xs, gaussian(xs), 'k-', lw=2)

# Get the upper and lower 2.5% centiles
lower,upper = np.percentile(resid, [2.5,97.5])
print('Lower: {l}. Upper: {u}'.format(l=lower, u=upper))

ys = plt.ylim()
plt.plot([lower]*2, [0.0, gaussian(lower)], 'k:')
plt.plot([upper]*2, [0.0, gaussian(upper)], 'k:')

plt.xlabel('Logarithmic residual')
plt.ylabel('Frequncy density')

if args.fig:
    plt.savefig(Resources.figure(args.fig))
    print('Output written in {where}'.format(where=Resources.figure(args.fig)))
else:
    plt.show()
