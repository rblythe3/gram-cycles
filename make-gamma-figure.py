#!/usr/bin/env python3

"""
Generate plots of the fixation time distribution from the numerical solution
of the Wright-Fisher model and compare to a Gamma distribution.
This is Figure 3 in the main text.
"""

import matplotlib.pyplot as plt, numpy as np, argparse
from scipy.special import gamma
from matplotlib import gridspec
from OriginFixation import Resources

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--fig', nargs='?', const='Fig3.eps', help='Output figure to file [off]')
args = parser.parse_args()

fig = plt.figure(3, figsize=(6,3))
gs = gridspec.GridSpec(1,2, left=0.15, right=0.95, bottom=0.20, wspace=0.1) #, right=0.95, top=0.95, height_ratios=[0.65,0.55,1], wspace=0.05, hspace=0.05)

axs = []
axs += [fig.add_subplot(gs[0])]
axs += [fig.add_subplot(gs[1], sharex=axs[0], sharey=axs[0])]
# plt.setp(axs[0].get_xticklabels(), visible=False)
plt.setp(axs[1].get_yticklabels(), visible=False)


for n,(N,s) in enumerate([('100','0'),('150','1e-2')]):
    t, cumfix = np.loadtxt(Resources.wrightfisher(N,'0',s,condition=True), unpack=True)
    # Convert the cumulative joint fixation distribution to a conditioned fixation pdf
    fix = (cumfix[1:]-cumfix[:-1])/cumfix[-1]
    # Shift ts to midpoints
    ts = (t[:-1]+t[1:])/2
    axs[n].plot(ts, fix, lw=1.5, ls='--', label='Wright-Fisher')
    # Determine mean and variance
    bart = np.average(ts, weights=fix)
    vart = np.average(ts**2, weights=fix) - bart**2
    # Convert to alpha and beta in the Gamma distribution
    alpha = bart**2/vart
    beta = bart / vart
    axs[n].plot(ts, beta**alpha / gamma(alpha) * ts**(alpha-1) * np.exp(-beta*ts), lw=2, label='Gamma')
    axs[n].set_xlabel(r'time, $t$')
    axs[n].text(0.04,0.9,chr(ord('a')+n),transform=axs[n].transAxes,weight='bold')


axs[0].set_ylabel(r'fixation probability, $p_{\rm F}(t)$')
axs[1].legend()

if args.fig:
    plt.savefig(Resources.figure(args.fig))
    print('Output written in {where}'.format(where=Resources.figure(args.fig)))
else:
    plt.show()
