#!/usr/bin/python3
"""
Run the Wright-Fisher simulation, storing the output in a sensible location.
"""

import argparse, os.path, subprocess
from OriginFixation import Resources

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--popsize', '-N', default='100', help='Population size [100]')
parser.add_argument('--mutate', '-m', default='1e-3', help='Mutation rate [1e-3]')
parser.add_argument('--selection', '-s', default='0', help='Selectrion strength [0]')
parser.add_argument('--twice', '-x', action='store_true', help='Allow second mutation [off]')
parser.add_argument('--target', '-T', default='1', choices=['1','2'], help='Fixation state to target: 1|2 [1]')
parser.add_argument('--condition', '-C', action='store_true', help='Condition on fixation [off]')
parser.add_argument('--generations', '-g', default='5000', help='Number of generations to run for [5000]')
parser.add_argument('--delta', '-d', default=1, help='Interval between writing to file [1]')
parser.add_argument('--dryrun', action='store_true', help='Just display the command that will be run [off]')
parser.add_argument('--clobber', action='store_true', help='Overwrite existing files [off]')

args = parser.parse_args()

if args.target == '2' and not args.twice:
    print("You cannot target the second mutant when only the first mutation step is allowed")
    exit(1)

datfile = Resources.wrightfisher(N=args.popsize, mu=args.mutate, s=args.selection, mutate_twice=args.twice, target=args.target, condition=args.condition)
datexists = os.path.exists(datfile)

cmd = [
    './wfbme-double',
    '-N{N}'.format(N=args.popsize),
    '-s{s}'.format(s=args.selection),
    '-m{m}'.format(m='0' if args.condition else args.mutate),
    '-M{M}'.format(M=args.mutate if args.twice else '0'),
    '-g{g}'.format(g=args.generations),
    '-d{d}'.format(d=args.delta)
]

if args.target=='2':
    cmd += ['--double']

if args.condition:
    cmd += ['--condition']

print(' '.join(cmd), '>', datfile)

if datexists:
    if args.clobber:
        if args.dryrun:
            print('\t(would overwrite)')
        else:
            with open(datfile, 'w') as fh:
                subprocess.call(cmd, stdout=fh)
    else:
        print('\t(would skip, file exists)' if args.dryrun else '\t(skipping, file exists)')
else:
    if args.dryrun:
        print('\t(would create)')
    else:
        with open(datfile, 'w') as fh:
            subprocess.call(cmd, stdout=fh)
