#!/usr/bin/python3

"""
Generate the table summarising the historical language data.
"""

import argparse
from OriginFixation import Resources
from Presentation import LongTable, Format

parser = argparse.ArgumentParser(description=__doc__)

args = parser.parse_args()

# Retrieve the data that are to be tabulated
definite = Resources.retrieve(Resources.languagechanges('definite'))
indefinite = Resources.retrieve(Resources.languagechanges('indefinite'))

def period(p):
    "Format a historical period specified as (start, end) in years relative to 1CE"
    def itoy(i):
        if i<0:
            return str(abs(i))+'BCE'
        elif i==0:
            return '1CE'
        else:
            return str(i)+'CE'

    return r' -- '.join(map(itoy, map(int, p)))

def seq(init ,states):
    "Fomat a sequence of language states"
    return ','.join(map(lambda x: str(x%4), range(init, init+states)))

def cite(refs):
    "Create LaTeX citation commands"
    return r'\cite{'+ ','.join(x[0] for x in refs) +'}' if refs else ''

# Now create the table writer
table = LongTable.LongTable(Resources.table('histories'),
    ['Language', 'Period', 'Definite', 'Indefinite', 'Weight', 'References'],
    caption = """
Empirical dataset of historical language changes. Period: the historical periods
over which observations were made. Definite, Indefinite: Stages of the
grammaticalisation cycles observed for each article. Weight: relative historical
average population size.
    """,
    label = 'stab:histories'
)

for dchange, ichange in zip(definite,indefinite):

    if dchange.language != ichange.language:
        print("Sequence of languages inconsistent between the two articles, bailing out")
        exit(1)

    if len(dchange.periods) == 1:
        table.row([
            Format.decamel(dchange.language),
            period(dchange.periods[0]),
            seq(dchange.initial, dchange.states),
            seq(ichange.initial, ichange.states),
            Format.to_precision(dchange.weight, 3),
            cite(dchange.references)
        ])
    else:
        table.splitrows(
            *(
            [ [ Format.decamel(dchange.language), period(dchange.periods[0])] ] +
            [ [period(x)] for x in dchange.periods[1:-1]] +
            [ [ period(dchange.periods[-1]),
                seq(dchange.initial, dchange.states),
                seq(ichange.initial, ichange.states),
                Format.to_precision(dchange.weight, 3),
                cite(dchange.references) ]
            ]
            )
        )

table.finalise()
print('Output written in {where}'.format(where=Resources.table('histories')))
