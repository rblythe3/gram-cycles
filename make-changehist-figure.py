#!/usr/bin/env python3

"""
Generate histograms of the estimated rate of change for each article and
langage in the data set.
This is Figure 2 in the main text.
"""

import matplotlib.pyplot as plt, argparse, numpy as np
from matplotlib import gridspec
from OriginFixation import Resources

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--fig', nargs='?', const='Fig2.eps', help='Output figure to file [off]')
args = parser.parse_args()

fig = plt.figure(2,figsize=(6,3))

gs = gridspec.GridSpec(1,2, bottom=0.15)
axs = []
axs += [fig.add_subplot(gs[0])]
axs += [fig.add_subplot(gs[1], sharey=axs[0])]
plt.setp(axs[1].get_yticklabels(), visible=False)

for n, article in enumerate(['definite', 'indefinite']):
    rates = [1000*lc.states / lc.years for lc in Resources.retrieve(Resources.languagechanges(article))]
    axs[n].hist(rates)
    axs[n].axvline(np.median(rates),c='k',ls=':')
    axs[n].set_xlabel('Number of changes / 1000y')
    axs[n].text(0.95,0.9,article,transform=axs[n].transAxes,ha='right')

axs[0].set_ylabel('Number of languages')


if args.fig:
    plt.savefig(Resources.figure(args.fig))
    print('Output written in {where}'.format(where=Resources.figure(args.fig)))
else:
    plt.show()
