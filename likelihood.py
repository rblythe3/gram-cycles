#!/usr/bin/python3
"""
Compute the likelihood of a set of language changes under certain model
assumptions.
"""

import argparse, numpy as np, re
from OriginFixation import Resources, Analysis, Summary

parser = argparse.ArgumentParser(description=__doc__)

parser.add_argument('model', choices=['poisson','wrightfisher'], nargs='?', default='poisson', help='Model assumption: poisson|wrightfisher [poisson]')
parser.add_argument('--feature', '-F', choices=['definite','indefinite'], default='definite', help='Language feature: definite|indefinite [definite]')
parser.add_argument('--reftime', '-t', default='-', help='Reference time point for language sizes [mean]')
parser.add_argument('--reflang', '-l', default='icelandic', help='Reference language to report parameters for [icelandic]')
parser.add_argument('--summary', '-S', help='Create summary with specified tag')
parser.add_argument('--montecarlo', '-M', action='store_true', help='Additionally create file for Monte Carlo sampling (Poisson only) [off]')

# Parameters relevant to an individual-based model
parser.add_argument('--selection', '-s', default='0', help='Selection strength [0] (can be inf)')
parser.add_argument('--interactionRate', '-R', default='0.04', help='Number of interactions per year [0.04]')
parser.add_argument('--storeSize', '-K', default='1', help='Number of memory slots [1]')
parser.add_argument('--memtime', '-m', help='Memory lifetime (overrides K if set)')
parser.add_argument('--network', '-n', default='inf', help='Network exponent [inf]')

args = parser.parse_args()

# Reload language changes,  stationary distribution and population growth curve
languagechanges = Resources.retrieve(Resources.languagechanges(args.feature))
stationary = Resources.retrieve(Resources.stationary(args.feature))
popcurve = Resources.retrieve(Resources.populationcurve())

reference = None # Reference language

# Freeze the language sizes...
if args.reftime == '-':
    # ... at their mean value over the specified time period
    for language in languagechanges:
        language.size(popcurve, moment=1, cache=True)
        if language.language.lower() == args.reflang.lower():
            reference = language
else:
    # ... or at a specific moment in time
    t = np.float_(args.reftime)
    for language in languagechanges:
        language.size(popcurve, t=t, cache=True)
        if language.language.lower() == args.reflang.lower():
            reference = language

if reference is None:
    print("Could not locate reference language '{reflang}' in data set".format(reflang=args.reflang))
    exit(1)

# Helper function to make a parameter mesh
def makemesh(spec):
    """
    Generate an array of points according to the supplied specification.

    Specification syntax: range|point[,range|point ...]
     range: start:end[:[scale]steps]
     point is numeric or 'inf'
     start, end, steps all numeric; scale is 'lin' or 'log'
    places each point in the mesh, and for each range, steps points from start to end in linear or log space
    in the case of log space, the endpoints and range cannot include zero.
    """
    mesh = np.empty((0,))
    for m in spec.split(','):
        if m == 'inf':
            mesh = np.append(mesh, np.float_('inf'))
        else:
            mat = re.match('([\d.e-]+)(?::([\d.e-]+))?(?::(lin|log)?(\d+)?)?',m)
            if mat is not None:
                beg,end,scl,bins = mat.groups()
                beg = np.float_(beg)
                if end is None or np.float_(end) == beg:
                    mesh = np.append( mesh, beg )
                    continue
                end = np.float_(end)
                if end < beg: beg,end = end,beg

                if scl is None: scl = 'lin'
                else: scl = scl.lower()
                if bins is None: bins = 50
                else: bins = int(bins)

                if scl == 'lin':
                    mesh = np.append( mesh, np.linspace(np.float_(beg), np.float_(end), bins) )
                elif scl == 'log':
                    if beg < 0.0:
                        if end >=0.0:
                            raise ValueError('Cannot span logarithmic bins across zero')
                        mesh = np.append( mesh, -np.logspace(np.log10(-beg),np.log10(-end),bins) )
                    elif beg > 0.0:
                        mesh = np.append( mesh, np.logspace(np.log10(beg),np.log10(end),bins) )
                    else:
                        raise ValueError('Cannot start logarithmic bins from zero')

    return mesh

# Determine where to write summaries out to (if at all)
summaryfile = Resources.summary(args.model, args.feature, args.summary) if args.summary else None

if args.model == 'poisson':
    # Fit a constant rate Poisson process and output the results
    summariser = Summary.Summariser(summaryfile, 'pop_innov_rate', 'log_likelihood', 'aicc', 'log_binary_overdispersion')
    analysis =  Analysis.Poisson(languagechanges, stationary, reference)
    results = analysis.analyse()
    summariser.summarise( results )
    analysis.finalise()
    summariser.close()

    if args.montecarlo:
        with open(Resources.montecarlo(args.feature), 'w') as fh:
            fh.write("{omega} {V} ".format(omega = results.pop_innov_rate, V = len(stationary)))
            fh.write(" ".join(map(str, stationary)))
            fh.write("\n")
            for language in languagechanges:
                fh.write("{initial} {transitions} {time}\n".format(
                    initial = language.initial,
                    transitions = language.states - 1,
                    time = language.years
                ))
        print("Wrote Monte Carlo input file to {where}".format(where=Resources.montecarlo(args.feature)))

elif args.model == 'wrightfisher':

    summariser = Summary.Summariser(summaryfile, 'selection', 'interaction_rate', 'memory_size', 'network_exponent', 'pop_innov_rate', 'ind_innov_rate', 'log_likelihood', 'aicc', 'log_binary_overdispersion')

    # Prepare ourselfs for both finite and infinite selection
    analysis_finites = Analysis.WrightFisher(languagechanges, stationary, reference)
    analysis_infinites = Analysis.InfiniteSelection(languagechanges, stationary, reference)

    # Loop around parameter values
    for s in makemesh(args.selection):
        # Select the appropriate analysis tool for the selection strength
        analysis = analysis_infinites if s == np.float_('inf') else analysis_finites

        for R in makemesh(args.interactionRate):
            for K in makemesh(args.storeSize):
                for nu in makemesh(args.network):
                    summariser.summarise(
                        analysis.analyse(
                            s=s, R=R, nu = nu,
                            K = R * np.float_(args.memtime) if args.memtime is not None else K
                        )
                    )

    analysis_finites.finalise()
    analysis_infinites.finalise()

    summariser.close()

else:
    # This check should already have happened in the arg parsing, but we'll include it for completeness
    print("Model '{model}' not recognised".format(model=args.model))
    exit(1)
