/*
* Run Monte Carlo simulations of the constant rate Poisson process for
* each of the languages in the sample to determine a Fisher p-value and
* a k-overdisperson index.
*
* Input file contains:
* First line: <mean-rate> <V> <stat1> <stat2> ... <statV>
* where mean-rate is the mean rate of the process (so omega_i = mean_rate / V f_i)
* V is the number of variants, and stat1 to statV are the stationary probabilities.
*
* For each language we then have: <initial> <transitions> <time>
* where initial is the initial state; transitions is the number of transitions; and time is the time window.
*
* We then sample the process for histsamples times to build up a histogram of
* the number of transitions (up to kmax).
*
* Using these histograms we then compute the mean and variance of each and
* therewith the overdispersion. We also resample from the histograms fishersamples
* times to estimate the Fisher p value.
*
*/

#include <iostream>
#include <vector>
#include <random>
#include <cmath>
#include <list>

#include "CLI11.hpp"

using WaitingTimeDist = std::exponential_distribution<double>;

class Language {
  unsigned initial;                               // Initial state
  unsigned transitions;                           // Number of transitions
  double time;                                    // Time over which these transitions took place
  std::vector<unsigned> hist;                     // hist[k] is number of times in MC sample that simulated language
  unsigned count;                                 // total number of samples in the histogram

  mutable std::discrete_distribution<unsigned> bootstrap; // random number distribution to generate bootstrap samples

public:
  Language(unsigned initial, unsigned transitions, unsigned time) :
    initial(initial), transitions(transitions), time(time), count(0) { } // Default construct everything else

  /**
    * Run the Poisson process from the initial condition for the language
    * up to the relevant time using the supplied random number generator,
    * sequence of waiting time distributions and generating the specified number of samples.
    * This builds up a histogram that we can then extract other statistics from.
    */
  template<typename Engine>
  void make_histogram(Engine& rng, unsigned samples, std::vector<WaitingTimeDist>& wtd) {
    for(unsigned n=0; n<samples; ++n) {
      unsigned state = initial;
      double t=0; // Time from initial state
      unsigned k=0; // Number of transitions
      for(;;) {
        t += wtd[state](rng);
        if(t>time) break;
        ++k;
        state = (state+1) % wtd.size();
      }
      if(k>=hist.size()) hist.resize(k+1);
      ++hist[k];
      ++count;
    }
    // Create/update a discrete distribution based on this histogram for bootstrap samples later
    bootstrap = std::discrete_distribution<unsigned>(hist.begin(), hist.end());
  }

  /**
    * Obtain the log likelihood of the actual outcome
    */
  double loglike_actual() const {
    return std::log(hist[transitions]) - std::log(count);
  }

  /**
    * Obtain the log likelihood for an outcome reconstructed from the histogram
    */
  template<typename Engine>
  double loglike_reconstructed(Engine& rng) const {
    return std::log(hist[bootstrap(rng)]) - std::log(count);
  }

  /**
    * Estimate the overdispersion of the number of transitions, on the assumption that
    * it is roughly Poisson distributed
    */
  double overdispersion() const {
    double kbar = 0.0, k2bar = 0.0;
    for(unsigned k=0; k<hist.size(); ++k) {
      double p = double(hist[k]) / double(count);
      kbar  += p*k;
      k2bar += p*k*k;
    }
    double dk = double(transitions) - kbar;
    double vark = k2bar - kbar*kbar;
    return dk*dk / vark;
  }
};

// Helper function to read a value from stdin, return true/false for success/fail
template<typename T>
bool readvalue(const std::string& name, T& val) {
  std::cin >> val;
  if(!std::cin.good()) {
    std::cerr << "Failed to read " << name << std::endl;
    return false;
  }
  return true;
}

int main(int argc, char* argv[]) {

  unsigned histsamples = 1000000;
  unsigned fishersamples = 1000000;
  unsigned seed = 0;

  CLI::App app{"Computation of k-overdisperson and Fisher p for a constant rate Poisson process"};

  app.add_option("-H,--histsamples", histsamples, "Histogram samples");
  app.add_option("-F,--fishersamples", fishersamples, "Fisher samples for p-value");
  app.add_option("-S,--seed", seed, "Random number seed");

  CLI11_PARSE(app, argc, argv);

  // Read mean rate of process
  double omega0;
  if(!readvalue("mean rate", omega0)) return 1;
  std::cout << "Mean rate: " << omega0 << std::endl;

  // Read in number of variants
  unsigned variants;
  if(!readvalue("number of variants", variants)) return 1;
  std::cout << "Number of variants: " << variants << std::endl;

  omega0 /= variants; // Mean rate out of state i is then omega0 / stationary[i]

  // Read in the stationary distribution and convert to a sequence of waiting time distributions
  std::vector<WaitingTimeDist> waitingtimes;
  for(unsigned n=0; n<variants; ++n) {
    double stationary;
    if(!readvalue("stationary probability", stationary)) return 1;
    waitingtimes.emplace_back(omega0 / stationary);
  }
  std::cout << "Transition rates: ";
  for(const auto& w: waitingtimes) std::cout << w.lambda() << " ";
  std::cout << std::endl;

  std::list<Language> languages;

  // Read in the languages until EOF
  for(;;) {
    unsigned initial, transitions;
    double time;
    std::cin >> initial >> transitions >> time;
    if(std::cin.eof()) break; // The only time this is allowed
    if(!std::cin.good()) {
      std::cerr << "Failed to read language properties" << std::endl;
      return 1;
    }
    // Initialise the language object
    languages.emplace_back(initial, transitions, time);
    // We'll wait until we've read everything correctly before wasting time on histogram generations
  }

  if(languages.empty()) {
    std::cerr << "No languages read, exiting";
    return 1;
  }

  std::cout << "Languages: " << languages.size() << std::endl;

  // Fire up a random number generator
  std::mt19937 rng(seed);

  // Now generate the histograms and calculate the empirical log likelihood
  // and the overdispersion
  double loglike = 0.0, sumover = 0.0;
  for(auto& l: languages) {
    l.make_histogram(rng, histsamples, waitingtimes);
    loglike += l.loglike_actual();
    double over = l.overdispersion();
    std::cout << "Partial overdispersion: " << over << std::endl;
    sumover += over;
  }
  std::cout << "Estimated data log likelihood: " << loglike << std::endl;
  std::cout << "Mean overdispersion: " << sumover / double(languages.size()) << std::endl;

  // Now reconstruct the distribution of histories by sampling from each histogram;
  // get the empirical log likelihood of that, and count the fraction that has a lower likelihood than
  // the actual history.
  unsigned outliers = 0;
  for(unsigned n=0; n<fishersamples; ++n) {
    // Generate fake sample and compute likelihood
    double fakeloglike = 0.0;
    for(const auto& l: languages) fakeloglike += l.loglike_reconstructed(rng);
    if(fakeloglike < loglike) ++outliers;
  }

  // outliers/fishersamples is estimate of p-value
  std::cout << "Estimated p-value: " << double(outliers)/double(fishersamples) << std::endl;

  return 0;
}
